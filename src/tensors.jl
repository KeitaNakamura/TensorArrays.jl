struct TensorArray{S <: Tuple, B <: Tuple{Vararg{AbstractSpace}}, T <: Real, N, L} <: AbstractArray{T, N}
    data::NTuple{L, T}

    function TensorArray{S, B, T, N, L}(x::Tuple, ::Symbol) where {S, B, T, N, L}
        new{S, B, T, N, L}(x)
    end
end

const TensorArrayNoEltype{S <: Tuple, B <: Tuple{Vararg{AbstractSpace}}, N, L, T <: Real} = TensorArray{S, B, T, N, L}

@inline function TensorArrayNoEltype{S, B, N, L}(x::Tuple, ::Symbol) where {S, B, N, L}
    return TensorArray{S, B, promote_type(typeof.(x)...), N, L}(x, :unsafe)
end

##################
# promote_tensor #
##################

@pure function promote_tensor(::Type{TensorArrayNoEltype{S, B, N, L}}) where {S, B, N, L}
    basis = Basis(S, B)
    if ndims(basis) != N
        return throw(ArgumentError("number of dimensions must match, expected $(ndims(basis)), got $N."))
    end
    if L != ncomponents(basis)
        return throw(ArgumentError("input length must match, expected $(ncomponents(basis)), got $L."))
    end
    return TensorArrayNoEltype{S, B, N, L}
end
@pure function promote_tensor(::Type{TensorArrayNoEltype{S, B, N}}) where {S, B, N}
    basis = Basis(S, B)
    L = ncomponents(basis)
    return promote_tensor(TensorArrayNoEltype{S, B, N, L})
end
@pure function promote_tensor(::Type{TensorArrayNoEltype{S, B}}) where {S, B}
    return promote_tensor(TensorArrayNoEltype{S, B, tuple_length(S)})
end
@pure function promote_tensor(::Type{TensorArray{S, B, T, N, L}}) where {S, B, T, N, L}
    promote_tensor(TensorArrayNoEltype{S, B, N, L}){T}
end
@pure function promote_tensor(::Type{TensorArray{S, B, T, N}}) where {S, B, T, N}
    promote_tensor(TensorArrayNoEltype{S, B, N}){T}
end
@pure function promote_tensor(::Type{TensorArray{S, B, T}}) where {S, B, T}
    promote_tensor(TensorArrayNoEltype{S, B}){T}
end

# tuple
@inline function (TA::Type{<: TensorArray})(x::Tuple)
    return promote_tensor(TA)(x, :unsafe)
end

# function
@generated function (::Type{TA})(f::Function) where {TA <: TensorArray}
    TensorType = promote_tensor(TA)
    exps = [:(f($(ij...))) for ij in tuplesubindexing(TensorType)]
    return quote
        @_inline_meta
        return $TensorType(tuple($(exps...)), :unsafe)
    end
end

@inline function create_tensor_by_linear_indices(f::Function, TA::Type{<: TensorArray})
    TensorType = promote_tensor(TA)
    inds = Tuple(tupleindexing(TensorType))
    return TensorType(f.(inds), :unsafe)
end

# array

# This function use Basis functor, not just conversion.
# For example, `SymmetricTensor{2, 3}(x)` is the same as `symmetric(x)`.
@generated function (::Type{TA})(x::AbstractArray) where {TA <: TensorArray}
    T = eltype(TA)
    return quote
        @_inline_meta
        basis = Basis(TA)
        basis($T, x)
    end
end

# rand, ones, zero

for (op, el) in ((:rand, :(_ -> rand(T))), (:ones, :(one(T))), (:zero, :(zero(T))))
    _op = Symbol(:_, op)
    @eval begin
        @inline Base.$op(TA::Type{<: TensorArray}) = $_op(promote_tensor(TA))
        @inline $_op(TA::Type{TensorArray{S, B, T, N, L}}) where {S, B, T, N, L} = fill($el, TA)
        @inline $_op(TA::Type{TensorArrayNoEltype{S, B, N, L}}) where {S, B, N, L} = $_op(TA{Float64})
        @inline Base.$op(x::TensorArray) = $op(typeof(x))
    end
end
@inline Base.fill(el::Real, TA::Type{<: TensorArray}) = create_tensor_by_linear_indices(i -> el, TA)
@inline Base.fill(el::Function, TA::Type{<: TensorArray}) = create_tensor_by_linear_indices(el, TA)

@inline ncomponents(TA::Type{<: TensorArray}) = ncomponents(Basis(TA))
@inline ncomponents(x::TensorArray) = ncomponents(typeof(x))

# necessary for AbstractArray

@inline Base.size(x::Type{<: TensorArray}) = size(Basis(x))
@inline Base.size(x::TensorArray) = size(typeof(x))
@inline function Base.size(x::Type{<: TensorArray}, i::Int)
    dims = size(x)
    return i > length(dims) ? 1 : dims[i]
end
@inline Base.size(x::TensorArray, i::Int) = size(typeof(x), i)

@inline Base.length(x::Type{<: TensorArray}) = prod(size(x))
@inline Base.length(x::TensorArray) = length(typeof(x))

Base.IndexStyle(::Type{<: TensorArray}) = IndexLinear()

@inline Base.Tuple(x::TensorArray) = x.data

@inline function Base.getindex(x::TensorArray, i::Int)
    @boundscheck checkbounds(x, i)
    @inbounds begin 
        index = tensorindexing(Basis(x))[i]
        return index > 0 ?  Tuple(x)[ index] :
               index < 0 ? -Tuple(x)[-index] :
                            zero(eltype(x))
    end
end

# static getindex

@inline function getindex_expr(ex::Union{Symbol, Expr}, x::Type{<: TensorArray}, i::Int)
    index = tensorindexing(Basis(x))[i]
    return index > 0 ? :( Tuple($ex)[$( index)]) :
           index < 0 ? :(-Tuple($ex)[$(-index)]) :
                       :(zero(eltype($ex)))
end

@inline function getindex_expr(ex::Union{Symbol, Expr}, x::Type{<: TensorArray}, i::Int, jk::Int...)
    getindex_expr(ex, x, LinearIndices(size(x))[i, jk...])
end

###########
# Aliases #
###########

# Vec

const Vec{dim, T <: Real} = TensorArray{Tuple{dim}, Tuple{Space{1}}, T, 1, dim}

function Base.summary(io::IO, x::Vec{dim, T}) where {dim, T}
    print(io, Base.dims2string(size(x)), " TensorArrays.Vec{$dim,$T}")
end

# Tensor, SymmetricTensor, AntisymmetricTensor
# FullySymmetricTensor, FullyAntisymmetricTensor

for (TensorType, BasisType) in ((:Tensor, Space),
                                (:SymmetricTensor, SymmetricSpace{2}),
                                (:AntisymmetricTensor, AntisymmetricSpace{2}),
                                (:FullySymmetricTensor, :(SymmetricSpace{N})),
                                (:FullyAntisymmetricTensor, :(AntisymmetricSpace{N})))
    @eval begin
        const $TensorType{N, dim, T <: Real, L} = TensorArray{NTuple{N, dim}, Tuple{$BasisType{N}}, T, N, L}
        function Base.summary(io::IO, x::$TensorType{N, dim, T, L}) where {N, dim, T, L}
            print(io, Base.dims2string(size(x)), " TensorArrays.", $(string(TensorType)), "{$N,$dim,$T,$L}")
        end
    end
    if TensorType == :Tensor || TensorType == :SymmetricTensor || TensorType == :AntisymmetricTensor
        if TensorType != :AntisymmetricTensor
            @eval @inline function Base.one(TA::Type{$TensorType{2, dim}}) where dim
                one($TensorType{2, dim, Float64})
            end
            @eval @inline function Base.one(TA::Type{$TensorType{2, dim, T}}) where {dim, T}
                z = zero(T)
                o = one(T)
                return TA(@inline function(i,j) i == j ? o : z end)
            end
        end
        @eval @inline function Base.one(TA::Type{$TensorType{4, dim}}) where dim
            one($TensorType{4, dim, Float64})
        end
        @eval @inline function Base.one(TA::Type{$TensorType{4, dim, T}}) where {dim, T}
            z = zero(T)
            o = one(T)
            basis = Basis(TA) # use Basis functor to handle SymmetricTensor and AntisymmetricTensor
            return basis(T, @inline function(i,j,k,l) i == k && j == l ? o : z end)
        end
    end
end
@inline function Base.one(t::TensorArray{S, B, T, N, L}) where {S, B, T, N, L}
    one(TensorArray{S, B, T, N})
end

# Levi-Civita

const LeviCivita{T <: Real} = TensorArray{NTuple{3, 3}, Tuple{AntisymmetricSpace{3, 3}}, T, 3, 1}

@inline function Base.one(::Type{LeviCivita})
    one(LeviCivita{Float64})
end
@inline function Base.one(TA::Type{LeviCivita{T}}) where T
    LeviCivita((-one(T),))
end
@inline function Base.one(x::LeviCivita{T}) where T
    one(LeviCivita{T})
end

@pure unsafe_basis(::Type{TensorArray{S, B, T, N, L}}) where {S, B, T, N, L} = Basis(S, B)
@pure unsafe_basis(::Type{TensorArrayNoEltype{S, B, N, L}}) where {S, B, N, L} = Basis(S, B)
@pure Basis(TA::Type{<: TensorArray}) = unsafe_basis(promote_tensor(TA))
@pure Basis(::Type{<: Real}) = Basis()
@inline Basis(x::Union{Real, TensorArray}) = Basis(typeof(x))

for func in (:tensorindexing, :tupleindexing, :tuplesubindexing)
    @eval begin
        @inline $func(TA::Type{<: TensorArray}) = $func(Basis(TA))
        @inline $func(x::TensorArray) = $func(typeof(x))
    end
end

@pure tensortype(TA::Type{<: TensorArray}) = tensortype(Basis(TA))
@inline tensortype(x::TensorArray) = tensortype(typeof(x))


const STensor{S <: Tuple, T <: Real, N, L} = TensorArray{S, Tuple{Space{N}}, T, N, L}
const STensorNoEltype{S <: Tuple, N, L, T <: Real} = TensorArray{S, Tuple{Space{N}}, T, N, L}

@pure function promote_tensor(::Type{STensorNoEltype{S}}) where S
    promote_tensor(STensorNoEltype{S, tuple_length(S)})
end
@pure function promote_tensor(::Type{STensor{S, T}}) where {S, T}
    promote_tensor(STensorNoEltype{S}){T}
end

function Base.summary(io::IO, x::STensor{S, T, N, L}) where {S, T, N, L}
    print(io, Base.dims2string(size(x)), " TensorArrays.STensor{$S,$T,$N,$L}")
end
