@testset "basic ops" begin
    for (TensorType, N, dim, T) in @eachtensor((1,2,3), (1,2,3), (Float32, Float64))
        x = rand(TensorType{N, dim, T})
        y = rand(Tensor{N, dim, Int})
        RetType_x = TensorType{N, dim, T}
        RetType_y = Tensor{N, dim, T}
        # unary
        @test (@inferred +x)::RetType_x == zero(x) + x
        @test (@inferred -x)::RetType_x == zero(x) - x
        # binary
        @test (@inferred x + x)::RetType_x == Array(x) + Array(x)
        @test (@inferred x + y)::RetType_y == Array(x) + Array(y)
        @test (@inferred y + x)::RetType_y == Array(y) + Array(x)
        @test (@inferred x - x)::RetType_x == Array(x) - Array(x)
        @test (@inferred x - y)::RetType_y == Array(x) - Array(y)
        @test (@inferred y - x)::RetType_y == Array(y) - Array(x)
        @test (@inferred   T(2) * x)::RetType_x == T(2) * Array(x)
        @test (@inferred   T(2) * y)::RetType_y == T(2) * Array(y)
        @test (@inferred Int(2) * x)::RetType_x == T(2) * Array(x)
        @test (@inferred x *   T(2))::RetType_x == T(2) * Array(x)
        @test (@inferred y *   T(2))::RetType_y == T(2) * Array(y)
        @test (@inferred x * Int(2))::RetType_x == T(2) * Array(x)
        @test (@inferred x /   T(2))::RetType_x == Array(x) / T(2)
        @test (@inferred y /   T(2))::RetType_y == Array(y) / T(2)
        @test (@inferred x / Int(2))::RetType_x == Array(x) / T(2)
    end
end

@testset "uniform scaling" begin
    for (TensorType, N, dim, T) in @eachtensor((Tensor, SymmetricTensor, AntisymmetricTensor), (2,), (1,2,3,4), (Float32, Float64))
        x = rand(TensorType{N, dim, T})
        if TensorType <: AntisymmetricTensor
            RetType = Tensor{N, dim, T}
            δ = one(SymmetricTensor{N, dim, T})
        else
            RetType = TensorType{N, dim, T}
            δ = one(x)
        end
        I2 = 2I
        @test (@inferred x + I2)::RetType == x + 2δ
        @test (@inferred x - I2)::RetType == x - 2δ
        @test (@inferred I2 + x)::RetType == 2δ + x
        @test (@inferred I2 - x)::RetType == 2δ - x
        @test (@inferred I2 ⋅ x)::TensorType{N, dim, T} == 2δ ⋅ x
        @test (@inferred x ⋅ I2)::TensorType{N, dim, T} == x ⋅ 2δ
        @test (@inferred x ⊡ I2)::T == x ⊡ 2δ
        @test (@inferred I2 ⊡ x)::T == 2δ ⊡ x
    end
    for dim in (1, 2, 3, 4), T in (Float32, Float64)
        I2 = 2I
        x = rand(Vec{dim, T})
        @test (@inferred I2 ⋅ x) == 2x
        @test (@inferred x ⋅ I2) == 2x
    end
end

@testset "tr, mean, vol, dev, transpose, cross" begin
    # tr, mean, vol, dev, transpose
    for (TensorType, N, dim, T) in @eachtensor((2,), (1,2,3,4), (Float32, Float64))
        x = rand(TensorType{N, dim, T})
        (@inferred tr(x))::T == tr(Array(x))
        (@inferred mean(x))::T == mean(Array(x))
        dim != 3 && continue
        @test (@inferred vol(x))::TensorType{N, dim, T} ≈ tr(Array(x)) / dim * Matrix(I, dim, dim)
        @test (@inferred dev(x))::TensorType{N, dim, T} ≈ Array(x) - tr(Array(x)) / dim * Matrix(I, dim, dim)
        @test vol(x) + dev(x) ≈ x
        @test (@inferred transpose(x))::TensorType{N, dim, T} == Array(x)'
        @test (@inferred adjoint(x))::TensorType{N, dim, T} == Array(x)'
        @test (x')::TensorType{N, dim, T} == Array(x)'
    end
    # cross
    triple = (x, y, z) -> x ⋅ (y × z)
    for T in (Float32, Float64)
        ϵ = one(LeviCivita{T})
        x = rand(Vec{3, T})
        y = rand(Vec{3, T})
        z = rand(Vec{3, T})
        (@inferred x × y)::Vec{3, T}
        @test x × y ≈ -y × x ≈ ϵ ⊡ (x ⊗ y) ≈ (ϵ ⋅ y) ⋅ x
        @test  triple(x, y, z) ≈  triple(y, z, x) ≈  triple(z, x, y) ≈
              -triple(y, x, z) ≈ -triple(z, y, x) ≈ -triple(x, z, y)
        ω = rand(Vec{3, T})
        W = -ϵ ⋅ ω
        @test W ⋅ x ≈ ω × x
    end
end

@testset "symmetric, antisymmetric" begin
    for (TensorType, N, dim, T) in @eachtensor((FullySymmetricTensor, FullyAntisymmetricTensor,), (1,2,3,4), (1,2,3,4), (Float32, Float64))
        x = rand(Tensor{N, dim, T})
        if TensorType == FullySymmetricTensor
            y = (@inferred symmetric(x))::FullySymmetricTensor{N, dim, T}
            for i in 1:length(y)
                sub = Tuple(CartesianIndices(y)[i])
                @test y[i] ≈ sum(x[inds...] for inds in permutations(sub)) / factorial(N)
            end
        elseif TensorType == FullyAntisymmetricTensor
            y = (@inferred antisymmetric(x))::FullyAntisymmetricTensor{N, dim, T}
            for i in 1:length(y)
                sub = Tuple(CartesianIndices(y)[i])
                @test y[i] ≈ sum(levicivita(sub) * levicivita(tuple(inds...)) * x[inds...] for inds in permutations(sub)) / factorial(N)
            end
        end
        @test y == TensorType{N, dim, T}(x)
    end
    for T in (Float32, Float64)
        x = rand(Tensor{3, 3, T})
        sym = (@inferred (symmetric(e₃ ⊗ e₃) ⊗ e₃)(x))::TensorArray{NTuple{3, 3}, Tuple{SymmetricSpace{2, 2}, Space{1}}, T}
        ant = (@inferred (antisymmetric(e₃ ⊗ e₃) ⊗ e₃)(x))::TensorArray{NTuple{3, 3}, Tuple{AntisymmetricSpace{2, 2}, Space{1}}, T}
        @test sym + ant ≈ x
    end
    for T in (Float32, Float64)
        x = rand(Tensor{4, 3, T})
        sym = (@inferred (symmetric(e₃ ⊗ e₃) ⊗ e₃ ⊗ e₃)(x))::TensorArray{NTuple{4, 3}, Tuple{SymmetricSpace{2, 2}, Space{2}}, T}
        ant = (@inferred (antisymmetric(e₃ ⊗ e₃) ⊗ e₃ ⊗ e₃)(x))::TensorArray{NTuple{4, 3}, Tuple{AntisymmetricSpace{2, 2}, Space{2}}, T}
        @test sym + ant ≈ x
    end
end

@testset "contract" begin
    for (TensorType_x, N_x, dim_x, T_x) in @eachtensor((1,2,3), (1,2,3), (Float32, Float64))
        x = rand(TensorType_x{N_x, dim_x, T_x})
        @test (@inferred norm(x))::T_x ≈ norm(Array(x))
        for (TensorType_y, N_y, dim_y, T_y) in @eachtensor((1,2,3), (1,2,3), (Float32, Float64))
            dim_x == dim_y || continue
            n = N_x == N_y ? N_x : min(N_x, N_y)
            y = rand(TensorType_y{N_y, dim_y, T_y})
            @test (@inferred contract(x, y, Val(n))) ≈ contract_array(x, y, Val(n)) atol=1e-5
        end
    end
end

@testset "det, inv" begin
    for (TensorType, N, dim, T) in @eachtensor((Tensor, SymmetricTensor), (2,4), (1,2,3,4), (Float32, Float64))
        Random.seed!(1234)
        x = rand(TensorType{N, dim, T})
        if N == 2
            @test (@inferred det(x))::T ≈ det(Matrix(x))
            @test (@inferred inv(x))::TensorType{N, dim, T} ≈ inv(Array(x))
        else # N == 4
            if dim ≤ 3
                @test x ⊡ (@inferred inv(x))::TensorType{N, dim, T} ≈ one(x)
            end
        end
    end
end

@testset "power" begin
    f = function(x, p)
        p == 0 ? one(x) :
        p  > 0 ? mapreduce(identity, dot, ntuple(i->x, p)) :
                 mapreduce(inv, dot, ntuple(i->x, -p))
    end
    for (TensorType, N, dim, T) in @eachtensor((Tensor, SymmetricTensor, AntisymmetricTensor), (2,), (1,2,3,4), (Float32, Float64))
        for p in -5:5
            p ≤ 0 && TensorType <: AntisymmetricTensor && continue
            if TensorType == AntisymmetricTensor
                OddRetType = AntisymmetricTensor{N, dim, T}
                EvenRetType = SymmetricTensor{N, dim, T}
            else
                OddRetType = TensorType{N, dim, T}
                EvenRetType = TensorType{N, dim, T}
            end
            @eval begin
                x = rand($TensorType{$N, $dim, $T})
                pow = x -> x^$p
                if isodd($p)
                    @test (@inferred pow(x))::$OddRetType ≈ $f(x, $p)
                else
                    @test (@inferred pow(x))::$EvenRetType ≈ $f(x, $p)
                end
            end
        end
    end
end
