@inline Base.:+(x::TensorArray{S}, y::TensorArray{S}) where S = map(+, x, y)
@inline Base.:-(x::TensorArray{S}, y::TensorArray{S}) where S = map(-, x, y)
@inline Base.:*(y::Real, x::TensorArray) = map(x -> x*y, x)
@inline Base.:*(x::TensorArray, y::Real) = map(x -> x*y, x)
@inline Base.:/(x::TensorArray, y::Real) = map(x -> x/y, x)
@inline Base.:+(x::TensorArray) = x
@inline Base.:-(x::TensorArray) = map(-, x)

@inline LinearAlgebra.:+(x::TensorArray, y::UniformScaling) = _plus_uniform( x,  y.λ)
@inline LinearAlgebra.:-(x::TensorArray, y::UniformScaling) = _plus_uniform( x, -y.λ)
@inline LinearAlgebra.:+(x::UniformScaling, y::TensorArray) = _plus_uniform( y,  x.λ)
@inline LinearAlgebra.:-(x::UniformScaling, y::TensorArray) = _plus_uniform(-y,  x.λ)

@generated function _plus_uniform(x::TensorArray{Tuple{dim, dim}}, λ::Real) where dim
    basis = promote_basis(Basis(x), symmetric(eᵢ(dim) ⊗ eᵢ(dim)))
    TensorType = tensortype(basis)
    exps = map(tuplesubindexing(TensorType)) do ij
        i, j = ij
        ex = getindex_expr(:x, x, i, j)
        return i == j ? :($ex + λ) : ex
    end
    return quote
        @_inline_meta
        @inbounds return $TensorType(tuple($(exps...)))
    end
end

@inline LinearAlgebra.dot(x::UniformScaling, y::Vec) = x.λ * y
@inline LinearAlgebra.dot(x::UniformScaling, y::TensorArray{Tuple{m, n}}) where {m, n} = x.λ * y
@inline LinearAlgebra.dot(x::Vec, y::UniformScaling) = x * y.λ
@inline LinearAlgebra.dot(x::TensorArray{Tuple{m, n}}, y::UniformScaling) where {m, n} = x * y.λ

@inline dcontract(x::UniformScaling, y::TensorArray{Tuple{dim, dim}}) where dim = x.λ * tr(y)
@inline dcontract(x::TensorArray{Tuple{dim, dim}}, y::UniformScaling) where dim = tr(x) * y.λ

@generated function contract_expr(x::Basis{Sx}, y::Basis{Sy}, ::Val{n}) where {Sx, Sy, n}
    nx = ndims(Sx)
    ny = ndims(Sy)
    ij_x = nindex(1, nx)
    ij_y = nindex(nx + 1 - n, nx + ny - n)
    if ij_x == ij_y
        return quote
            @inbounds return @einsum x[$(ij_x...)] * y[$(ij_y...)]
        end
    else
        ij_z = [ij_x[1:end-n]; ij_y[(1+n):end]]
        return quote
            @inbounds return @einsum ($(ij_z...),) -> x[$(ij_x...)] * y[$(ij_y...)]
        end
    end
end

"""
    contract(x::TensorArray, y::TensorArray, ::Val{n})

Compute the `n`th order contraction between two tensors `x` and `y`.

# Example

```jldoctest
julia> x = rand(STensor{Tuple{2,2,3}})
2×2×3 TensorArrays.STensor{Tuple{2,2,3},Float64,3,12}:
[:, :, 1] =
 0.590845  0.566237
 0.766797  0.460085

[:, :, 2] =
 0.794026  0.200586
 0.854147  0.298614

[:, :, 3] =
 0.246837  0.648882
 0.579672  0.0109059

julia> y = rand(STensor{Tuple{2,3,2}})
2×3×2 TensorArrays.STensor{Tuple{2,3,2},Float64,3,12}:
[:, :, 1] =
 0.066423  0.646691  0.276021
 0.956753  0.112486  0.651664

[:, :, 2] =
 0.0566425  0.950498  0.945775
 0.842714   0.96467   0.789904

julia> contract(x, y, Val(2))
2×2 TensorArrays.Tensor{2,2,Float64,4}:
 1.60803  2.20487
 1.24419  2.08794
```
"""
@generated function contract(x::TensorArray, y::TensorArray, ::Val{n}) where n
    sx = Basis(x)
    sy = Basis(y)
    sz = contract(sx, sy, Val(n))
    ex = contract_expr(sx, sy, Val(n))
    ElType = promote_type(eltype(x), eltype(y))
    if isa(ex, Expr)
        @assert sz === Basis()
        return quote
            @_inline_meta
            ElType = $ElType
            @inbounds return $ex
        end
    else
        TensorType = tensortype(sz)
        return quote
            @_inline_meta
            ElType = $ElType
            @inbounds return $TensorType(tuple($(ex[tupleindexing(sz)]...)))
        end
    end
end

"""
    otimes(x::TensorArray, y::TensorArray)
    ⊗(x::TensorArray, y::TensorArray)

Compute the tensor product between two tensors `x` and `y`.
This is equivalent to `contract(x, y, Val(0))`.
The symbol `⊗`, written `\\otimes`, is overloaded for the tensor product.

# Example

```jldoctest
julia> x = rand(Vec{3})
3-element TensorArrays.Vec{3,Float64}:
 0.5908446386657102
 0.7667970365022592
 0.5662374165061859

julia> y = rand(Vec{2})
2-element TensorArrays.Vec{2,Float64}:
 0.4600853424625171
 0.7940257103317943

julia> x ⊗ y
3×2 TensorArrays.STensor{Tuple{3,2},Float64,2,6}:
 0.271839  0.469146
 0.352792  0.608857
 0.260518  0.449607
```
"""
@inline otimes(x::TensorArray, y::TensorArray) = contract(x, y, Val(0))

"""
    dot(x::TensorArray, y::TensorArray)
    ⋅(x::TensorArray, y::TensorArray)

Compute the single contraction between two tensors `x` and `y`.
This is equivalent to `contract(x, y, Val(1))`.
The symbol `⋅`, written `\\cdot`, is overloaded for the single contraction.

# Example

```jldoctest
julia> x = rand(SymmetricTensor{2,2})
2×2 TensorArrays.SymmetricTensor{2,2,Float64,3}:
 0.590845  0.766797
 0.766797  0.566237

julia> y = rand(Vec{2})
2-element TensorArrays.Vec{2,Float64}:
 0.4600853424625171
 0.7940257103317943

julia> x ⋅ y
2-element TensorArrays.Vec{2,Float64}:
 0.8806955195116766
 0.8023991439961493
```
"""
@inline LinearAlgebra.dot(x::TensorArray, y::TensorArray) = contract(x, y, Val(1))

"""
    dcontract(x::TensorArray, y::TensorArray)
    ⊡(x::TensorArray, y::TensorArray)

Compute the double contraction between two tensors `x` and `y`.
This is equivalent to `contract(x, y, Val(2))`.
The symbol `⊡`, written `\\boxdot`, is overloaded for the double contraction.

# Example

```jldoctest
julia> x = rand(Tensor{3,2})
2×2×2 TensorArrays.Tensor{3,2,Float64,8}:
[:, :, 1] =
 0.590845  0.566237
 0.766797  0.460085

[:, :, 2] =
 0.794026  0.200586
 0.854147  0.298614

julia> y = rand(SymmetricTensor{2,2})
2×2 TensorArrays.SymmetricTensor{2,2,Float64,3}:
 0.246837  0.579672
 0.579672  0.648882

julia> x ⊡ y
2-element TensorArrays.Vec{2,Float64}:
 1.0645058505682097
 1.144863198158123
```
"""
@inline dcontract(x::TensorArray, y::TensorArray) = contract(x, y, Val(2))

"""
    norm(::TensorArray)

Compute the norm of a tensor.

# Example

```jldoctest
julia> x = rand(Tensor{2,3})
3×3 TensorArrays.Tensor{2,3,Float64,9}:
 0.590845  0.460085  0.200586
 0.766797  0.794026  0.298614
 0.566237  0.854147  0.246837

julia> norm(x)
1.7377443667834922
```
"""
@inline function LinearAlgebra.norm(x::TensorArray{S, B, T, N}) where {S, B, T, N}
    sqrt(contract(x, x, Val(N)))
end

"""
    tr(::TensorArray{Tuple{dim, dim} where dim})

Compute the trace of a second order tensor.

# Example

```jldoctest
julia> x = rand(Tensor{2,3})
3×3 TensorArrays.Tensor{2,3,Float64,9}:
 0.590845  0.460085  0.200586
 0.766797  0.794026  0.298614
 0.566237  0.854147  0.246837

julia> tr(x)
1.6317075356075135
```
"""
@generated function LinearAlgebra.tr(x::TensorArray{Tuple{dim, dim}}) where dim
    return quote
        @_inline_meta
        ElType = eltype(x)
        @inbounds return $(@einsum x[i,i])
    end
end
@inline Statistics.mean(x::TensorArray{Tuple{dim, dim}}) where dim = tr(x) / dim

"""
    vol(::TensorArray{Tuple{3, 3}})

Compute the volumetric part of a second order tensor
based on the additive decomposition.

# Example

```jldoctest
julia> x = rand(Tensor{2,3})
3×3 TensorArrays.Tensor{2,3,Float64,9}:
 0.590845  0.460085  0.200586
 0.766797  0.794026  0.298614
 0.566237  0.854147  0.246837

julia> vol(x)
3×3 TensorArrays.Tensor{2,3,Float64,9}:
 0.543903  0.0       0.0
 0.0       0.543903  0.0
 0.0       0.0       0.543903
```
"""
@inline function vol(x::TensorArray{NTuple{2, 3}})
    v = mean(x)
    z = zero(v)
    return tensortype(x)(@inline function(i,j) i == j ? v : z end)
end

"""
    dev(::TensorArray{Tuple{3, 3}})

Compute the deviatoric part of a second order tensor
based on the additive decomposition.

# Example

```jldoctest
julia> x = rand(Tensor{2,3})
3×3 TensorArrays.Tensor{2,3,Float64,9}:
 0.590845  0.460085  0.200586
 0.766797  0.794026  0.298614
 0.566237  0.854147  0.246837

julia> dev(x)
3×3 TensorArrays.Tensor{2,3,Float64,9}:
 0.0469421  0.460085   0.200586
 0.766797   0.250123   0.298614
 0.566237   0.854147  -0.297065
```
"""
@inline dev(x::TensorArray{NTuple{2, 3}}) = x - vol(x)

"""
    symmetric(::TensorArray)

Compute the symmetric part of a tensor.

# Example

```jldoctest
julia> x = rand(Tensor{2,3})
3×3 TensorArrays.Tensor{2,3,Float64,9}:
 0.590845  0.460085  0.200586
 0.766797  0.794026  0.298614
 0.566237  0.854147  0.246837

julia> symmetric(x)
3×3 TensorArrays.SymmetricTensor{2,3,Float64,6}:
 0.590845  0.613441  0.383412
 0.613441  0.794026  0.57638
 0.383412  0.57638   0.246837
```
"""
@inline function symmetric(x::TensorArray)
    basis = symmetric(Basis(x))
    return basis(x)
end

"""
    antisymmetric(::TensorArray)

Compute the antisymmetric part of a tensor.

# Example

```jldoctest
julia> x = rand(Tensor{2,3})
3×3 TensorArrays.Tensor{2,3,Float64,9}:
 0.590845  0.460085  0.200586
 0.766797  0.794026  0.298614
 0.566237  0.854147  0.246837

julia> antisymmetric(x)
3×3 TensorArrays.AntisymmetricTensor{2,3,Float64,3}:
 0.0       -0.153356  -0.182826
 0.153356   0.0       -0.277766
 0.182826   0.277766   0.0
```
"""
@inline function antisymmetric(x::TensorArray)
    basis = antisymmetric(Basis(x))
    return basis(x)
end

@generated function Base.transpose(x::TensorArray{Tuple{m, n}, B, T, N, L}) where {m, n, B, T, N, L}
    exps = [:(x[$j, $i]) for (i,j) in tuplesubindexing(x)]
    return quote
        @_inline_meta
        @inbounds return TensorArray{Tuple{n, m}, B, T, N, L}(tuple($(exps...)))
    end
end
@inline Base.adjoint(x::TensorArray) = transpose(x)

"""
    det(::TensorArray{Tuple{dim, dim} where dim})

Compute the determinant of a second order tensor.

# Example

```jldoctest
julia> x = rand(Tensor{2,3})
3×3 TensorArrays.Tensor{2,3,Float64,9}:
 0.590845  0.460085  0.200586
 0.766797  0.794026  0.298614
 0.566237  0.854147  0.246837

julia> det(x)
-0.0029960171394771556
```
"""
@generated function LinearAlgebra.det(x::TensorArray{Tuple{1, 1}})
    return quote
        @_inline_meta
        @inbounds return $(getindex_expr(:x, x, 1))
    end
end
@generated function LinearAlgebra.det(x::TensorArray{Tuple{2, 2}})
    x_11 = getindex_expr(:x, x, 1, 1)
    x_21 = getindex_expr(:x, x, 2, 1)
    x_12 = getindex_expr(:x, x, 1, 2)
    x_22 = getindex_expr(:x, x, 2, 2)
    return quote
        @_inline_meta
        @inbounds return $x_11 * $x_22 - $x_12 * $x_21
    end
end
@generated function LinearAlgebra.det(x::TensorArray{Tuple{3, 3}})
    x_11 = getindex_expr(:x, x, 1, 1)
    x_21 = getindex_expr(:x, x, 2, 1)
    x_31 = getindex_expr(:x, x, 3, 1)
    x_12 = getindex_expr(:x, x, 1, 2)
    x_22 = getindex_expr(:x, x, 2, 2)
    x_32 = getindex_expr(:x, x, 3, 2)
    x_13 = getindex_expr(:x, x, 1, 3)
    x_23 = getindex_expr(:x, x, 2, 3)
    x_33 = getindex_expr(:x, x, 3, 3)
    return quote
        @_inline_meta
        @inbounds return $x_11 * ($x_22*$x_33 - $x_23*$x_32) -
                         $x_12 * ($x_21*$x_33 - $x_23*$x_31) +
                         $x_13 * ($x_21*$x_32 - $x_22*$x_31)
    end
end
@inline function LinearAlgebra.det(x::TensorArray{Tuple{dim, dim}}) where dim
    det(Matrix(x))
end

"""
    inv(::TensorArray{Tuple{dim, dim} where dim})

Compute the inverse of a second order tensor.

# Example

```jldoctest
julia> x = rand(Tensor{2,3})
3×3 TensorArrays.Tensor{2,3,Float64,9}:
 0.590845  0.460085  0.200586
 0.766797  0.794026  0.298614
 0.566237  0.854147  0.246837

julia> inv(x)
3×3 TensorArrays.Tensor{2,3,Float64,9}:
  19.7146   -19.2802    7.30384
   6.73809  -10.7687    7.55198
 -68.541     81.4917  -38.8361
```
"""
@inline function Base.inv(x::TensorArray{Tuple{1, 1}})
    tensortype(x)((1/det(x),))
end
@generated function Base.inv(x::TensorArray{Tuple{2, 2}})
    x_11 = getindex_expr(:x, x, 1, 1)
    x_21 = getindex_expr(:x, x, 2, 1)
    x_12 = getindex_expr(:x, x, 1, 2)
    x_22 = getindex_expr(:x, x, 2, 2)
    exps = [:($x_22 * idet), :(-$x_21 * idet), :(-$x_12 * idet), :($x_11 * idet)]
    return quote
        @_inline_meta
        idet = 1 / det(x)
        TensorType = tensortype(x)
        @inbounds return TensorType(tuple($(exps[tupleindexing(x)]...)))
    end
end
@generated function Base.inv(x::TensorArray{Tuple{3, 3}})
    x_11 = getindex_expr(:x, x, 1, 1)
    x_21 = getindex_expr(:x, x, 2, 1)
    x_31 = getindex_expr(:x, x, 3, 1)
    x_12 = getindex_expr(:x, x, 1, 2)
    x_22 = getindex_expr(:x, x, 2, 2)
    x_32 = getindex_expr(:x, x, 3, 2)
    x_13 = getindex_expr(:x, x, 1, 3)
    x_23 = getindex_expr(:x, x, 2, 3)
    x_33 = getindex_expr(:x, x, 3, 3)
    exps = [:(($x_22*$x_33 - $x_23*$x_32) * idet),
            :(($x_23*$x_31 - $x_21*$x_33) * idet),
            :(($x_21*$x_32 - $x_22*$x_31) * idet),
            :(($x_13*$x_32 - $x_12*$x_33) * idet),
            :(($x_11*$x_33 - $x_13*$x_31) * idet),
            :(($x_12*$x_31 - $x_11*$x_32) * idet),
            :(($x_12*$x_23 - $x_13*$x_22) * idet),
            :(($x_13*$x_21 - $x_11*$x_23) * idet),
            :(($x_11*$x_22 - $x_12*$x_21) * idet)]
    return quote
        @_inline_meta
        idet = 1 / det(x)
        TensorType = tensortype(x)
        @inbounds return TensorType(tuple($(exps[tupleindexing(x)]...)))
    end
end
@inline function Base.inv(x::TensorArray{Tuple{dim, dim}}) where dim
    tensortype(x)(inv(Matrix(x)))
end


"""
    inv(::Tensor{4, dim where dim})
    inv(::SymmetricTensor{4, dim where dim})

Compute the inverse of a fourth order tensor.

# Example

```jldoctest
julia> x = rand(Tensor{4,3});

julia> x ⊡ inv(x) ≈ one(Tensor{4,3})
true

julia> x = rand(SymmetricTensor{4,3});

julia> x ⊡ inv(x) ≈ one(SymmetricTensor{4,3})
true
```
"""
@inline function Base.inv(x::Tensor{4, dim}) where dim
    fromvoigt(Tensor{4, dim}, inv(tovoigt(x)))
end
@inline function Base.inv(x::SymmetricTensor{4, dim}) where dim
    frommandel(SymmetricTensor{4, dim}, inv(tomandel(x)))
end

"""
    cross(x::Vec{3}, y::Vec{3})

Compute the cross product between two vectors `x` and `y`.

# Example

```jldoctest
julia> x = rand(Vec{3})
3-element TensorArrays.Vec{3,Float64}:
 0.5908446386657102
 0.7667970365022592
 0.5662374165061859

julia> y = rand(Vec{3})
3-element TensorArrays.Vec{3,Float64}:
 0.4600853424625171
 0.7940257103317943
 0.8541465903790502

julia> x × y
3-element TensorArrays.Vec{3,Float64}:
  0.20535000738340053
 -0.24415039787171888
  0.11635375677388776
```
"""
@inline function LinearAlgebra.cross(x::Vec{3}, y::Vec{3})
    @inbounds return Vec{3}((x[2]*y[3] - x[3]*y[2],
                             x[3]*y[1] - x[1]*y[3],
                             x[1]*y[2] - x[2]*y[1]))
end

@inline Base.literal_pow(::typeof(^), x::TensorArray{Tuple{dim, dim}}, ::Val{-1}) where dim = inv(x)
@inline Base.literal_pow(::typeof(^), x::TensorArray{Tuple{dim, dim}}, ::Val{0}) where dim = one(x)
@inline Base.literal_pow(::typeof(^), x::TensorArray{Tuple{dim, dim}}, ::Val{1}) where dim = x
@inline Base.literal_pow(::typeof(^), x::TensorArray{Tuple{dim, dim, dim, dim}}, ::Val{-1}) where dim = inv(x)
@generated function Base.literal_pow(::typeof(^), x::TensorArray{Tuple{dim, dim}} where dim, ::Val{p}) where {p}
    ex = p > 0 ? :(_literal_pow(x, $(Val(p)))) : :(_literal_pow(inv(x), $(Val(-p))))
    return quote
        @_inline_meta
        return $ex
    end
end
@generated function _literal_pow(x::TensorArray{Tuple{dim, dim}} where dim, ::Val{p}) where {p}
    if isodd(p)
        ex = :(_powdot(x, x²^$((p-1)÷2)))
    else
        ex = :(x²^$(p÷2))
    end
    return quote
        @_inline_meta
        x² = _powdot(x, x)
        return $ex
    end
end

# @inline function Base.:^(A::TensorArray{Tuple{dim, dim}}, p::Integer) where {dim}
    # p < 0 ? Base.power_by_squaring(inv(A), -p) : Base.power_by_squaring(A, p)
# end
# function Base.power_by_squaring(x::TensorArray{Tuple{dim, dim}}, p::Integer) where {dim}
    # x² = _powdot(x, x)
    # if p == 1
        # return x
    # elseif p == 0
        # return one(x)
    # elseif isodd(p)
        # return _powdot(x, x²^((p-1)÷2))
    # else
        # return x²^(p÷2)
    # end
# end

@inline _powdot(x::Tensor{2, dim}, y::Tensor{2, dim}) where dim = x ⋅ y
@generated function _powdot(x::SymmetricTensor{2, dim}, y::SymmetricTensor{2, dim}) where dim
    TensorType = SymmetricTensor{2, dim}
    ElType = promote_type(eltype(x), eltype(y))
    exps = (@einsum (i,j) -> x[i,k] * y[k,j])[tupleindexing(TensorType)]
    return quote
        @_inline_meta
        ElType = $ElType
        @inbounds return $TensorType(tuple($(exps...)))
    end
end
@generated function _powdot(x::AntisymmetricTensor{2, dim}, y::AntisymmetricTensor{2, dim}) where dim
    TensorType = SymmetricTensor{2, dim}
    ElType = promote_type(eltype(x), eltype(y))
    exps = (@einsum (i,j) -> x[i,k] * y[k,j])[tupleindexing(TensorType)]
    return quote
        @_inline_meta
        ElType = $ElType
        @inbounds return $TensorType(tuple($(exps...)))
    end
end
@generated function _powdot(x::AntisymmetricTensor{2, dim}, y::SymmetricTensor{2, dim}) where dim
    TensorType = AntisymmetricTensor{2, dim}
    ElType = promote_type(eltype(x), eltype(y))
    exps = (@einsum (i,j) -> x[i,k] * y[k,j])[tupleindexing(TensorType)]
    return quote
        @_inline_meta
        ElType = $ElType
        @inbounds return $TensorType(tuple($(exps...)))
    end
end
