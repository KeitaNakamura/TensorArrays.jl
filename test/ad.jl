@testset "AD" begin
    @testset "real -> real" begin
        for T in (Float32, Float64, Int)
            x = rand(T)
            @test (@inferred gradient(x -> 2x^2 - 3x + 2, x))::T == 4x - 3
            @test (@inferred gradient(x -> 2x^2 - 3x + 2, x, :all))::Tuple{T, T} == (4x - 3, 2x^2 - 3x + 2)
            @test (@inferred gradient(x -> 3, x))::Int == zero(T)
            @test (@inferred gradient(x -> 3, x, :all))::Tuple{Int, Int} == (zero(T), 3)
        end
    end
    @testset "real -> tensor" begin
        for (TensorType, N, dim, T) in @eachtensor((Tensor, SymmetricTensor, AntisymmetricTensor), (1,2,3), (1,2,3,4), (Float32, Float64))
            RetType = TensorType{N, dim}
            @eval begin
                x = rand($T)
                @test (@inferred gradient(x -> $RetType((ij...) -> prod(ij) * x^2), x))::$RetType{$T} == $RetType((ij...) -> prod(ij) * 2x)
                @test (@inferred gradient(x -> $RetType((ij...) -> prod(ij) * x^2), x, :all))::Tuple{$RetType{$T}, $RetType{$T}} == ($RetType((ij...) -> prod(ij) * 2x), $RetType((ij...) -> prod(ij) * x^2))
                @test (@inferred gradient(x -> $RetType((ij...) -> prod(ij)), x))::$RetType{Int} == zero($RetType)
                @test (@inferred gradient(x -> $RetType((ij...) -> prod(ij)), x, :all))::Tuple{$RetType{Int}, $RetType{Int}} == (zero($RetType), $RetType((ij...) -> prod(ij)))
            end
        end
    end
    @testset "tensor -> real" begin
        I₁ = x -> tr(x)
        I₂ = x -> (tr(x)^2 - tr(x^2)) / 2
        I₃ = x -> det(x)
        for (TensorType, N, dim, T) in @eachtensor((Tensor, SymmetricTensor), (2,), (1,2,3,4), (Float32, Float64))
            Random.seed!(1234)
            RetType = TensorType{N, dim, T}
            x = rand(TensorType{N, dim, T})
            @test (@inferred gradient(I₁, x))::RetType ≈ one(x)
            @test (@inferred gradient(I₂, x))::RetType ≈ I₁(x)*one(x) - x'
            @test (@inferred gradient(I₃, x))::RetType ≈ det(x)*inv(x)'
            @test (@inferred gradient(I₁, x, :all))[1]::RetType ≈ one(x)
            @test (@inferred gradient(I₂, x, :all))[1]::RetType ≈ I₁(x)*one(x) - x'
            @test (@inferred gradient(I₃, x, :all))[1]::RetType ≈ det(x)*inv(x)'
            @test (@inferred gradient(I₁, x, :all))[2]::T ≈ I₁(x)
            @test (@inferred gradient(I₂, x, :all))[2]::T ≈ I₂(x)
            @test (@inferred gradient(I₃, x, :all))[2]::T ≈ I₃(x)
            @test (@inferred gradient(x -> 1, x))::TensorType{N, dim, Int} == zero(x)
            @test (@inferred gradient(x -> 1, x, :all))::Tuple{TensorType{N, dim, Int}, Int} == (zero(x), 1)
        end
    end
    @testset "tensor -> tensor" begin
        for (TensorType, N, dim, T) in @eachtensor((Tensor, SymmetricTensor, AntisymmetricTensor), (2,), (1,2,3,4), (Float32, Float64))
            x = rand(TensorType{N, dim, T})
            RetType = TensorType{4, dim, T}
            @test (@inferred gradient(x -> 2x, x))::RetType == 2*one(RetType)
            @test (@inferred gradient(x -> 2x, x, :all))::Tuple{RetType, TensorType{N, dim, T}} == (2*one(RetType), 2x)

            if TensorType == Tensor
                RetType = TensorArray{NTuple{4, dim}, Tuple{Space{4}}, T}
            elseif TensorType == SymmetricTensor
                RetType = TensorArray{NTuple{4, dim}, Tuple{Space{2}, SymmetricSpace{2, 2}}, T}
            else # TensorType == AntisymmetricTensor
                RetType = TensorArray{NTuple{4, dim}, Tuple{Space{2}, AntisymmetricSpace{2, 2}}, T}
            end
            y = rand(Tensor{N, dim, T})
            @eval begin
                @test (@inferred gradient(x -> $y, $x))::$RetType == zero($y) ⊗ zero($x)
                @test (@inferred gradient(x -> $y, $x, :all))::Tuple{$RetType, Tensor{$N, $dim, $T}} == (zero($y) ⊗ zero($x), $y)
            end
        end
    end
end
