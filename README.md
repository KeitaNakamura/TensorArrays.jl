# TensorArrays

[![travis status](https://travis-ci.org/KeitaNakamura/TensorArrays.jl.svg?branch=master)](https://travis-ci.org/KeitaNakamura/TensorArrays.jl)
[![appveyor status](https://ci.appveyor.com/api/projects/status/9yp8snyo5760pep9/branch/master?svg=true)](https://ci.appveyor.com/project/KeitaNakamura/tensorarrays-jl/branch/master)

[![Coverage Status](https://coveralls.io/repos/github/KeitaNakamura/TensorArrays.jl/badge.svg?branch=master)](https://coveralls.io/github/KeitaNakamura/TensorArrays.jl?branch=master)
[![codecov status](http://codecov.io/github/KeitaNakamura/TensorArrays.jl/coverage.svg?branch=master)](http://codecov.io/github/KeitaNakamura/TensorArrays.jl?branch=master)

[![docs_latest](https://img.shields.io/badge/docs-latest-blue.svg)](https://KeitaNakamura.github.io/TensorArrays.jl/latest)
