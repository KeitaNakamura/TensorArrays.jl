abstract type AbstractSpace{I, N} end

@inline Base.ndims(::Type{<: AbstractSpace{I, N}}) where {I, N} = N
@inline Base.ndims(::AbstractSpace{I, N}) where {I, N} = N
@inline Base.length(x::AbstractSpace) = prod(size(x))
@inline Base.size(x::AbstractSpace) = x.dims
@inline function Base.size(x::AbstractSpace, i::Int)
    dims = size(x)
    @inbounds return i > length(dims) ? 1 : dims[i]
end
@inline Base.LinearIndices(x::AbstractSpace) = LinearIndices(size(x))
@inline Base.CartesianIndices(x::AbstractSpace) = CartesianIndices(size(x))

function Base.show(io::IO, v::AbstractSpace{I, N}) where {I, N}
    print(io, reduce((x, y) -> "$x ⊗ $y", split(v)))
end

struct Space{N} <: AbstractSpace{1, N}
    dims::NTuple{N, Int}
end
@inline Space(dim::Vararg{Int}) = Space(dim)

function Base.show(io::IO, x::Space{1})
    print(io, "e")
    dim = size(x, 1)
    foreach(string(dim)) do i
        num = 0x2080 + UInt16(parse(Int, i))
        print(io, Char(num))
    end
end

for name in ("Symmetric", "Antisymmetric")
    SpaceType = Symbol(name, :Space)
    name = Symbol(lowercase(name))
    @eval begin
        struct $SpaceType{I, N} <: AbstractSpace{I, N}
            dims::NTuple{N, Int}
            function $SpaceType{I, N}(dims::NTuple{N, Int}) where {I, N}
                if !(N % I == 0 && N ≥ 2 && I ≥ 2)
                    throw(ArgumentError("invalid space type $($SpaceType){$N, $I}."))
                end
                space = new{I::Int, N::Int}(dims)
                split(space) # check dimentions
                return space
            end
            function $SpaceType{N, N}(dims::NTuple{N, Int}) where {N}
                if !allsame(dims)
                    throw(ArgumentError("all dimentions must be the same for $($SpaceType), got '$dims'."))
                end
                if !(N ≥ 2)
                    throw(ArgumentError("invalid space type $($SpaceType){$N, $N}."))
                end
                if $SpaceType === AntisymmetricSpace && !(2 ≤ N ≤ dims[1])
                    throw(ArgumentError("number of components of $($SpaceType){$N, $N} is zero."))
                end
                return new{N::Int, N::Int}(dims)
            end
        end
        @inline $SpaceType{I}(dims::NTuple{N, Int}) where {I, N} = $SpaceType{I, N}(dims)
        @inline $SpaceType{I}(dims::Vararg{Int}) where {I} = $SpaceType{I}(dims)
        @inline $SpaceType(dims::NTuple{N, Int}) where {N} = $SpaceType{N, N}(dims)
        @inline $SpaceType(dims::Vararg{Int}) = $SpaceType(dims)

        function Base.show(io::IO, x::$SpaceType{N, N}) where N
            print(io, "$($name)(", Space(size(x)), ")")
        end

        @inline function Base.reduce(x::$SpaceType{I}, y::$SpaceType{I}) where I
            $SpaceType{I}((size(x)..., size(y)...)) |> tuple
        end

        # dimensions must match
        @generated function promote_space(x::$SpaceType{N1, N1}, y::$SpaceType{N2, N2}) where {N1, N2}
            :(@_inline_meta; $(N1 < N2 ? :x : :y))
        end

        @inline $name(x::Space) = $SpaceType(size(x))
    end
end

@inline ncomponents(x::Space) = prod(size(x))
@inline function ncomponents(x::SymmetricSpace{N, N}) where {N}
    d = size(x, 1)
    return binomial(d + N - 1, N)
end
@inline function ncomponents(x::AntisymmetricSpace{N, N}) where {N}
    d = size(x, 1)
    return binomial(d, N)
end

@inline Base.reduce(x::AbstractSpace, y::AbstractSpace) = (x, y)
@inline function Base.reduce(x::Space, y::Space)
    Space((size(x)..., size(y)...)) |> tuple
end
@generated function Base.reduce(x::NTuple{N, AbstractSpace}, y::AbstractSpace) where N
    exps = [:(x[$i]) for i in 1:N-1]
    return quote
        @inbounds return tuple($(exps...), reduce(x[$N], y)...)
    end
end
@generated function Base.reduce(x::NTuple{N, AbstractSpace}) where N
    ex = N > 1 ? :(reduce(x[1], x[2])) : :x
    for i in 3:N
        ex = :(reduce($ex, x[$i]))
    end
    return quote
        @inbounds return $ex
    end
end

@generated function Base.split(x::AbstractSpace{I, N}) where {I, N}
    SpaceType = x.name.name
    exps = map(1:I:N) do i
        dims = Expr(:tuple, [:(x.dims[$j]) for j in i:i+I-1]...)
        return :($SpaceType($dims))
    end
    return quote
        @inbounds return tuple($(exps...))
    end
end

@inline droplast(x::Space{1}) = ()
@inline function droplast(x::AbstractSpace{2, 2})
    dim = size(x, 1)
    return (Space(dim),)
end
@generated function droplast(x::AbstractSpace{N, N}) where N
    SpaceType = nameof(x)
    exps = [:(dims[$i]) for i in 1:N-1]
    return quote
        @_inline_meta
        dims = size(x)
        @inbounds return ($SpaceType(tuple($(exps...))),)
    end
end
@generated function droplast(s::NTuple{N, AbstractSpace}, ::Val{1} = Val(1)) where N
    exps = [:(s[$i]) for i in 1:N-1]
    push!(exps, Expr(:..., :(droplast(s[$N]))))
    return quote
        @_inline_meta
        tuple($(exps...))
    end
end
@generated function droplast(s::Tuple{Vararg{AbstractSpace}}, ::Val{N}) where N
    :(droplast(droplast(s), Val($(N-1))))
end

@inline indexing(::Space{1}, i::Tuple{Int}) = @inboundsret i[1]
@inline function indexing(x::SymmetricSpace{N, N}, ij::NTuple{N, Int}) where N
    inds = reverse(tuple_sort(ij))
    return @inbounds LinearIndices(x)[inds...]
end
@inline function indexing(x::AntisymmetricSpace{N, N}, ij::NTuple{N, Int}) where N
    inds = reverse(tuple_sort(ij)) # `reverse` is for column-major order
    linear = LinearIndices(x)
    if levicivita(inds) == -1 # sorted inds is odd permutation
        @inbounds rootidx = linear[inds...]
    else # When sorted inds is even permutation, change it to odd permutation
        @inbounds rootidx = linear[inds[2], inds[1], inds[3:N]...]
    end
    -levicivita(ij) * rootidx
end

#################
# promote_space #
#################

# dimensions must be checked in advance

@inline promote_space(x::Space{1}, y::Space{1}) = x
@generated function promote_space(x::AbstractSpace{I1, N1}, y::AbstractSpace{I2, N2}) where {I1, N1, I2, N2}
    ex = N1 < N2 ? :x : :y
    return :(@_inline_meta; Space(size($ex)))
end

@inline function promote_space(s1::NTuple{N1, AbstractSpace}, s2::NTuple{N2, AbstractSpace}) where {N1, N2}
    _promote_space(s1, s2, ())
end
@inline function _promote_space(s1::NTuple{N1, AbstractSpace}, s2::NTuple{N2, AbstractSpace}, promoted::Tuple) where {N1, N2}
    s = promote_space(s1[N1], s2[N2])
    n = Val(ndims(s))
    _promote_space(droplast(s1, n), droplast(s2, n), (s, promoted...))
end
@inline _promote_space(s1::Tuple{}, s2::Tuple{}, promoted::Tuple{Vararg{AbstractSpace}}) = promoted



struct SpaceSet{N, S <: NTuple{N, AbstractSpace}}
    spaces::S
    function SpaceSet(s::Tuple{Vararg{AbstractSpace}})
        spaces = reduce(s)
        N = length(spaces)
        return new{N, typeof(spaces)}(spaces)
    end
end
@inline SpaceSet(spaces::Vararg{AbstractSpace}) = SpaceSet(spaces)

@inline Base.split(x::SpaceSet) = flatten(split, x.spaces)

@inline droplast(s::SpaceSet) = SpaceSet(droplast(split(s)))

@inline dropfirst(s::SpaceSet) = reverse(s) |> droplast |> reverse
@inline Base.reverse(s::SpaceSet) = SpaceSet(reverse(split(s)))

@inline ncomponents(s::SpaceSet) = mapreduce(ncomponents, *, split(s); init = 1)
@inline Base.size(s::SpaceSet) = flatten(size, split(s))
@inline function Base.size(x::SpaceSet, i::Int)
    dims = size(x)
    @inbounds return i > length(dims) ? 1 : dims[i]
end
@inline Base.ndims(s::SpaceSet) = mapreduce(ndims, +, split(s); init = 0)
@inline Base.length(s::SpaceSet) = prod(size(s))

@inline function promote_space(x::SpaceSet, y::SpaceSet)
    size(x) == size(y) || throw(DimensionMismatch("dimensions must match"))
    SpaceSet(promote_space(split(x), split(y)))
end

@inline symmetric(x::SpaceSet{1, <: Tuple{Space}}) = SpaceSet(SymmetricSpace(size(x)))
@inline antisymmetric(x::SpaceSet{1, <: Tuple{Space}}) = SpaceSet(AntisymmetricSpace(size(x)))

@inline spacetype(::SpaceSet{N, S}) where {N, S} = S

Base.show(io::IO, s::SpaceSet) = print(io, "SpaceSet", s.spaces)
