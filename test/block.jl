@testset "BlockArray" begin
    @testset "tomandel_tuple/frommandel_tuple" begin
        for dim in (1,2,3), T in (Float32, Float64)
            A = rand(Tensor{2, dim, T})
            AA = rand(Tensor{4, dim, T})
            A_sym = rand(SymmetricTensor{2, dim, T})
            AA_sym = rand(SymmetricTensor{4, dim, T})
            order = vec(TensorArrays.VOIGT_ORDER[dim])
            @test SVector(@inferred tomandel_tuple(A)) ≈ tovoigt(A)[order]
            @test SVector(@inferred tomandel_tuple(AA)) ≈ vec(tovoigt(AA)[order, order])
            order = vec(TensorArrays.VOIGT_ORDER[dim]')
            order = order[order .≤ sum(1:dim)]
            @test SVector(@inferred tomandel_tuple(A_sym)) ≈ tomandel(A_sym)[order]
            @test SVector(@inferred tomandel_tuple(AA_sym)) ≈ vec(tomandel(AA_sym)[order, order])
        end
        for (TensorType, N, dim, T) in @eachtensor((Tensor, SymmetricTensor, AntisymmetricTensor), (1,2,3), (1,2,3,4), (Float32, Float64))
            x = rand(TensorType{N, dim, T})
            L = ncomponents(x)
            data = (@inferred tomandel_tuple(x))::NTuple{L, T}
            @test (@inferred frommandel_tuple(TensorType{N, dim}, data))::TensorType{N, dim, T} ≈ x
            @test (@inferred frommandel_tuple(TensorType{N, dim, T}, data))::TensorType{N, dim, T} ≈ x
        end
    end

    @testset "AD" begin
        TensorArrays.otimes(x, y) = x * y
        TensorArrays.ncomponents(x::Real) = 1
        @generated function test_sum(x::BlockVector{S}) where S
            Expr(:call, :+, [:(sum(x[$(Val(i))])) for i in 1:length(S)]...)
        end
        for T in (Float32, Float64)
            x1 = 1
            x2 = rand(Tensor{2, 3, T})
            x3 = rand(SymmetricTensor{2, 3, T})
            spaces = (Basis(x1), Basis(x2), Basis(x3))
            BlockVectorType = BlockVector{spaces, T, Tuple{T, tensortype(x2){T}, tensortype(x3){T}}} where {T <: Real}

            # constructor, getindex, setindex!
            x = (@inferred BlockVector(x1, x2, x3))::BlockVectorType{T}
            @test (@inferred x[Val(1)])::T ≈ x1
            @test (@inferred x[Val(2)])::Tensor{2, 3, T} ≈ x2
            @test (@inferred x[Val(3)])::SymmetricTensor{2, 3, T} ≈ x3
            x[Val(1)] = x1 = 2
            x[Val(2)] = x2 = rand(Tensor{2, 3, T})
            x[Val(3)] = x3 = rand(SymmetricTensor{2, 3, T})
            @test (@inferred x[Val(1)])::T ≈ x1
            @test (@inferred x[Val(2)])::Tensor{2, 3, T} ≈ x2
            @test (@inferred x[Val(3)])::SymmetricTensor{2, 3, T} ≈ x3

            @test norm(x) ≈ sqrt(x[Val(1)]^2 + x[Val(2)] ⊡ x[Val(2)] + x[Val(3)] ⊡ x[Val(3)])

            x1_x1 = T(x1 ⊗ x1); x1_x2 = x1 ⊗ x2; x1_x3 = x1 ⊗ x3
            x2_x1 = x2 ⊗ x1;    x2_x2 = x2 ⊗ x2; x2_x3 = x2 ⊗ x3
            x3_x1 = x3 ⊗ x1;    x3_x2 = x3 ⊗ x2; x3_x3 = x3 ⊗ x3

            BlockMatrixType = BlockMatrix{spaces, spaces, T,
                                          Tuple{T,                    tensortype(x2_x1){T}, tensortype(x3_x1){T},
                                                tensortype(x1_x2){T}, tensortype(x2_x2){T}, tensortype(x3_x2){T},
                                                tensortype(x1_x3){T}, tensortype(x2_x3){T}, tensortype(x3_x3){T}}} where {T <: Real}

            # constructor
            (@inferred BlockMatrix{spaces, spaces}(x1_x1, x2_x1, x3_x1,
                                                   x1_x2, x2_x2, x3_x2,
                                                   x1_x3, x2_x3, x3_x3))::BlockMatrixType{T}

            ## AD
            # BlockVector -> BlockVector (Dual)
            y = (@inferred gradient(identity, x))::BlockMatrixType{T}
            @test inv(inv(y)) ≈ y

            res = (@inferred gradient(identity, x, :all))::Tuple{typeof(y), typeof(x)}
            @test res[1] ≈ y
            @test res[2] ≈ x

            @test (@inferred y[Val(1), Val(1)]) ≈  one(x1_x1); @test (@inferred y[Val(1), Val(2)]) ≈ zero(x1_x2); @test (@inferred y[Val(1), Val(3)]) ≈ zero(x1_x3)
            @test (@inferred y[Val(2), Val(1)]) ≈ zero(x2_x1); @test (@inferred y[Val(2), Val(2)]) ≈  one(x2_x2); @test (@inferred y[Val(2), Val(3)]) ≈ zero(x2_x3)
            @test (@inferred y[Val(3), Val(1)]) ≈ zero(x3_x1); @test (@inferred y[Val(3), Val(2)]) ≈ zero(x3_x2); @test (@inferred y[Val(3), Val(3)]) ≈  one(x3_x3)

            # BlockVector -> BlockVector (non-Dual)
            z = @eval (@inferred gradient(x -> $(BlockVector(x1, x2, x3)), $x))::typeof($y)
            for i in 1:length(z)
                @test z[Val(i)] ≈ zero(y[Val(i)])
            end

            # BlockVector -> Dual
            z, v = (@inferred gradient(test_sum, x, :all))::Tuple{BlockVectorType{T}, T}
            @test z[Val(1)] ≈ 1.0
            for i in 2:length(z)
                @test z[Val(i)] ≈ ones(y[Val(i)])
            end
            @test z ≈ (@inferred gradient(test_sum, x))::BlockVectorType{T}
            @test v ≈ test_sum(x)

            # BlockVector -> non-Dual
            z, v = (@inferred gradient(x -> 1, x, :all))::Tuple{BlockVectorType{Int}, Int}
            for i in 1:length(z)
                @test z[Val(i)] ≈ zero(y[Val(i)])
            end
            @test z ≈ (@inferred gradient(x -> 1, x))::BlockVectorType{Int}
            @test v ≈ 1
        end
        for T in (Float32, Float64)
            x1 = 1
            x2 = rand(Vec{3, T})
            spaces = (Basis(x1), Basis(x2))
            BlockVectorType = BlockVector{spaces, T, Tuple{T, tensortype(x2){T}}} where {T <: Real}

            # constructor, getindex, setindex
            x = (@inferred BlockVector(x1, x2))::BlockVectorType{T}
            @test (@inferred x[Val(1)])::T ≈ x1
            @test (@inferred x[Val(2)])::Vec{3, T} ≈ x2
            x[Val(1)] = x1 = 2
            x[Val(2)] = x2 = rand(x2)
            @test (@inferred x[Val(1)])::T ≈ x1
            @test (@inferred x[Val(2)])::Vec{3, T} ≈ x2

            @test norm(x) ≈ sqrt(x[Val(1)]^2 + x[Val(2)] ⋅ x[Val(2)])

            x1_x1 = T(x1 ⊗ x1); x1_x2 = x1 ⊗ x2
            x2_x1 = x2 ⊗ x1;    x2_x2 = x2 ⊗ x2

            BlockMatrixType = BlockMatrix{spaces, spaces, T,
                                          Tuple{T,                    tensortype(x2_x1){T},
                                                tensortype(x1_x2){T}, tensortype(x2_x2){T}}} where {T <: Real}

            # constructor
            (@inferred BlockMatrix{spaces, spaces}(x1_x1, x2_x1, x1_x2, x2_x2))::BlockMatrixType{T}

            ## AD
            # BlockVector -> BlockVector (Dual)
            y = (@inferred gradient(identity, x))::BlockMatrixType{T}
            @test inv(inv(y)) ≈ y

            res = (@inferred gradient(identity, x, :all))::Tuple{typeof(y), typeof(x)}
            @test res[1] ≈ y
            @test res[2] ≈ x

            @test (@inferred y[Val(1), Val(1)]) ≈  one(x1_x1); @test (@inferred y[Val(1), Val(2)]) ≈ zero(x1_x2)
            @test (@inferred y[Val(2), Val(1)]) ≈ zero(x2_x1); @test (@inferred y[Val(2), Val(2)]) ≈  one(x2_x2)

            y[Val(1), Val(1)] = y11 = rand(T);     y[Val(1), Val(2)] = y12 = rand(x1_x2)
            y[Val(2), Val(1)] = y21 = rand(x2_x1); y[Val(2), Val(2)] = y22 = rand(x2_x2)
            @test (@inferred y[Val(1), Val(1)]) ≈ y11; @test (@inferred y[Val(1), Val(2)]) ≈ y12
            @test (@inferred y[Val(2), Val(1)]) ≈ y21; @test (@inferred y[Val(2), Val(2)]) ≈ y22

            # BlockVector -> BlockVector (non-Dual)
            z = @eval (@inferred gradient(x -> $(BlockVector(x1, x2)), $x))::typeof($y)
            for i in 1:length(z)
                @test z[Val(i)] ≈ zero(y[Val(i)])
            end

            # BlockVector -> Dual
            z, v = (@inferred gradient(test_sum, x, :all))::Tuple{BlockVectorType{T}, T}
            @test z[Val(1)] ≈ 1.0
            for i in 2:length(z)
                @test z[Val(i)] ≈ ones(y[Val(i)])
            end
            @test z ≈ (@inferred gradient(test_sum, x))::BlockVectorType{T}
            @test v ≈ test_sum(x)

            # BlockVector -> non-Dual
            z, v = (@inferred gradient(x -> 1, x, :all))::Tuple{BlockVectorType{Int}, Int}
            for i in 1:length(z)
                @test z[Val(i)] ≈ zero(y[Val(i)])
            end
            @test z ≈ (@inferred gradient(x -> 1, x))::BlockVectorType{Int}
            @test v ≈ 1
        end
    end
end
