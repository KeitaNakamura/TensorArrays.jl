macro inboundsret(ex)
    return quote
        @inbounds v = $(esc(ex))
        v
    end
end

@pure promote_tuple_type(x::Type{<: Tuple{Vararg{Real}}}) = promote_type(x.parameters...)

@inline tuple_sort(x::NTuple{N, Int}) where N = Tuple(sort!(MVector{N, Int}(x)))
@pure tuple_length(x::Type{<: Tuple}) = length(x.parameters)

@generated function flatten(x::Tuple{Vararg{Tuple, N}}) where N
    exps = [Expr(:..., :(x[$i])) for i in 1:N]
    return quote
        @_inline_meta
        @inbounds return tuple($(exps...))
    end
end
@inline flatten(f::Function, x::Tuple) = flatten(f.(x))

@inline allsame(v) = all(x -> x == v[1], v)

function levicivita(x::Tuple{Vararg{Int, N}}) where N
    even = true
    @inbounds for i in 1:N, j in i+1:N
        x[i] == x[j] && return 0
        even ⊻= x[i] > x[j]
    end
    return even ? 1 : -1
end

@inline function nindex(start::Int, last::Int)
    [Symbol(:i_, j) for j in start:last]
end
