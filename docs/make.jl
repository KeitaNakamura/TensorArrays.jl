using Documenter
using TensorArrays

makedocs(
    modules = [TensorArrays],
    format = :html,
    sitename = "TensorArrays.jl",
    doctest = true,
    pages = Any[
        "Home" => "index.md",
        "Manual" => [
            "man/ops.md",
        ],
    ]
)

deploydocs(
    repo = "github.com/KeitaNakamura/TensorArrays.jl.git",
    target = "build",
    osname = "linux",
    julia  = "0.6",
    deps = nothing,
    make = nothing
)
