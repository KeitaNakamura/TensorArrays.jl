struct Basis{S}
    function Basis{S}() where S
        new{S::SpaceSet}()
    end
end
@pure Basis(S::SpaceSet) = Basis{S}()
@pure Basis(s::Tuple{Vararg{AbstractSpace}}) = Basis(SpaceSet(s))
@pure Basis(s::Vararg{AbstractSpace}) = Basis(s)

@pure function Basis(::Type{Size}, ::Type{Spaces}) where {Size, Spaces}
    construct_basis(Size, Spaces)
end

@pure function construct_basis(::Type{Size}, ::Type{Spaces}) where {Size, Spaces}
    if !all(x -> isa(x, Int) && x > 0, Size.parameters)
        throw(ArgumentError("size parameter must be a Tuple of positive Ints (e.g. `Tuple{3,3}`), got $Size."))
    end
    if !all(x -> !isa(x, UnionAll), Spaces.parameters)
        throw(ArgumentError("space parameter must be a Tuple of space (e.g. `Tuple{SymmetricSpace{3,3}}`), got $Spaces."))
    end
    if length(Size.parameters) != mapreduce(ndims, +, Spaces.parameters, init = 0)
        throw(ArgumentError("number of dimensions must match, got size $Size and basis $Spaces."))
    end
    dims = tuple(Size.parameters...)
    stypes = tuple(Spaces.parameters...)
    st::Int = 1
    spaces = map(stypes) do s
        n = ndims(s)
        s(dims[st:(st+=n)-1])
    end
    basis = Basis(spaces)
    if spacetype(basis) !== Spaces
        throw(ArgumentError("use $(spacetype(basis)) for space parameter, got $Spaces"))
    end
    return basis
end

@pure Base.size(::Basis{S}) where {S} = size(S)
@pure Base.size(::Basis{S}, i::Int) where {S} = size(S, i)
@pure Base.length(::Basis{S}) where {S} = length(S)
@pure Base.ndims(::Basis{S}) where {S} = ndims(S)
@pure ncomponents(::Basis{S}) where {S} = ncomponents(S)
@pure spacetype(::Basis{S}) where {S} = spacetype(S)

@pure droplast(::Basis{S}) where {S} = Basis(droplast(S))
@pure droplast(x::Basis, ::Val{N}) where {N} = droplast(droplast(x), Val(N-1))
@pure droplast(x::Basis, ::Val{0}) = x
@pure dropfirst(::Basis{S}) where {S} = Basis(dropfirst(S))
@pure dropfirst(x::Basis, ::Val{N}) where {N} = dropfirst(dropfirst(x), Val(N-1))
@pure dropfirst(x::Basis, ::Val{0}) = x

@pure function contract(x::Basis, y::Basis, ::Val{N}) where {N}
    if !(0 ≤ N ≤ ndims(x) && 0 ≤ N ≤ ndims(y) && size(x)[end-N+1:end] === size(y)[1:N])
        throw(DimensionMismatch("dimensions must match"))
    end
    otimes(droplast(x, Val(N)), dropfirst(y, Val(N)))
end
@pure otimes(::Basis{S1}, ::Basis{S2}) where {S1, S2} = Basis(S1.spaces..., S2.spaces...)
@pure LinearAlgebra.dot(x::Basis, y::Basis) = contract(x, y, Val(1))
@pure dcontract(x::Basis, y::Basis) = contract(x, y, Val(2))

@pure function promote_basis(::Basis{S1}, ::Basis{S2}) where {S1, S2}
    if size(S1) != size(S2)
        throw(DimensionMismatch("dimensions must match"))
    end
    Basis(promote_space(S1, S2))
end
@pure promote_basis(x::Basis, y::Basis, z::Basis...) = promote_basis(promote_basis(x, y), z...)

@pure Base.LinearIndices(x::Basis) = LinearIndices(size(x))
@pure Base.CartesianIndices(x::Basis) = CartesianIndices(size(x))

@pure function indexing(::Basis{S}, ij::CartesianIndex{N}) where {S, N}
    @assert ndims(S) == N
    st::Int = 1
    spaces = split(S)
    inds = map(spaces) do s
        n = ndims(s)
        indexing(s, Tuple(ij)[st:(st+=n)-1])
    end
    linear = LinearIndices(length.(spaces))
    sgn = sign(prod(inds))
    @inbounds return sgn == 0 ? 0 : sgn * linear[abs.(inds)...]
end

@pure function indexing(x::Basis)
    @inbounds return SArray{Tuple{size(x)...}, Int}([indexing(x, ij) for ij in CartesianIndices(x)])
end

@pure function tensorindexing(x::Basis)
    dict = Dict{Int, Int}()
    SArray{Tuple{size(x)...}, Int}([i == 0 ? 0 : sign(i) * get!(dict, abs(i), length(dict) + 1) for i in indexing(x)])
end

@pure function tupleindexing(x::Basis)
    index = i -> indexing(x, CartesianIndices(x)[i])
    inds = Int[index(1)]
    @inbounds begin
        for i in 2:length(x)
            j = index(i)
            inds[end] < j && push!(inds, j)
        end
        inds[1] == 0 && popfirst!(inds)
    end
    return SVector{ncomponents(x), Int}(inds)
end

@pure function tuplesubindexing(x::Basis)
    SVector{ncomponents(x), typeof(size(x))}(map(i -> CartesianIndices(x)[i], tupleindexing(x)))
end

@pure function tupleindexdups(x::Basis)
    inds = tensorindexing(x)
    dups = Dict{Int, Int}()
    for i in eachindex(inds)
        ti = abs(inds[i])
        if !haskey(dups, ti)
            dups[ti] = 1
        else
            dups[ti] += 1
        end
    end
    N = ncomponents(x)
    return SVector{N, Int}([dups[i] for i in 1:N])
end

function Base.show(io::IO, ::Basis{S}) where {S}
    print(io, "Basis(", reduce((x, y) -> "$x ⊗ $y", split(S)), ")")
end
function Base.show(io::IO, ::Basis{SpaceSet()})
    print(io, "Basis(ℝ)")
end

@pure eᵢ(dim::Int) = Basis(Space(dim))

@pure symmetric(::Basis{S}) where {S} = Basis(symmetric(S))
@pure antisymmetric(::Basis{S}) where {S} = Basis(antisymmetric(S))

@generated function tensortype(::Basis{S}) where {S}
    dims = size(S)
    :(@_pure_meta; TensorArrayNoEltype{Tuple{$(dims...)}, $(spacetype(S)), $(length(dims)), $(ncomponents(S))})
end

@inline function (basis::Basis{S})(::Type{T}, f::Function) where {S, T <: Real}
    tensortype(basis){T}(basis_functor(basis, f))
end
@inline function (basis::Basis{S})(::Type{Any}, f::Function) where {S}
    tensortype(basis)(basis_functor(basis, f))
end
@inline (basis::Basis{S})(f::Function) where {S} = basis(Any, f)

@generated function basis_functor(::Basis{S}, f::Function) where {S}
    basis = Basis(S)
    dims = size(basis)
    tinds = tensorindexing(basis)
    dups = tupleindexdups(basis)
    exps = Vector{Expr}(undef, ncomponents(basis))
    @inbounds for i in 1:length(basis)
        ti = tinds[i]
        if ti != 0
            j = abs(ti)
            ij = Tuple(CartesianIndices(basis)[i])
            ex = dups[j] == 1 ?
                 :($(sign(ti)) * f($(ij...))) :
                 :($(sign(ti)) * f($(ij...)) / $(dups[j]))
            exps[j] = isassigned(exps, j) ? :($(exps[j]) + $ex) : ex
        end
    end
    return quote
        @_inline_meta
        return tuple($(exps...))
    end
end

@inline function (basis::Basis)(::Type{T}, x::AbstractArray) where T
    promote_shape(size(basis), size(x)) # check size
    basis(T, @inline function(ij...) @inboundsret x[ij...]; end)
end
@inline (basis::Basis)(x::AbstractArray) = basis(Any, x)
