mutable struct BlockArray{S <: Tuple, T <: Real, Ts <: Tuple{Vararg{Union{T, TensorArray{<: Tuple, <: Tuple, T}}}}}
    data::Ts
    function BlockArray{S, T, Ts}(x::Ts) where {S, T, Ts}
        check_block_parameters(S, Ts)
        new{S, T, Ts}(x)
    end
end

@generated function check_block_parameters(::Type{S}, ::Type{Ts}) where {S, Ts}
    if !all(x -> isa(x, Tuple{Vararg{Basis}}), S.parameters)
        return :(throw(ArgumentError("size parameter must be a Tuple of tuple of Spaces (e.g. `Tuple{(e₀, symmetric(e₃ ⊗ e₃))}`), got $S.")))
    end
    dims = tuple(map(length, S.parameters)...)
    if prod(dims) != length(Ts.parameters)
        return :(throw(ArgumentError("basis mismatch in Block Array parameters. Got spaces $S, arguments $Ts.")))
    end
    for i in 1:prod(dims)
        sub = CartesianIndices(dims)[i]
        basis = mapreduce(j -> S.parameters[j][sub[j]], ⊗, 1:length(sub); init = e₀)
        if basis != Basis(Ts.parameters[i])
            return :(throw(ArgumentError("basis mismatch in Block Array parameters. Got spaces $S, arguments $Ts.")))
        end
    end
end

const BlockVector{S, T <: Real, Ts <: Tuple{Vararg{Union{T, TensorArray{<: Tuple, <: Tuple, T, <: Any, <: Any}}}}} = BlockArray{Tuple{S}, T, Ts}
const BlockMatrix{Si, Sj, T <: Real, Ts <: Tuple{Vararg{Union{T, TensorArray{<: Tuple, <: Tuple, T, <: Any, <: Any}}}}} = BlockArray{Tuple{Si, Sj}, T, Ts}

@generated function BlockArray{S}(xs::NTuple{N, Union{Real, TensorArray}}) where {S <: Tuple, N}
    types = xs.parameters
    T = promote_type(map(eltype, types)...)
    exps = map(1:N) do i
        types[i] <: Real ? :($T(xs[$i])) : :($(tensortype(types[i]){T})(Tuple(xs[$i])))
    end
    return quote
        data = tuple($(exps...))
        BlockArray{S, $T, typeof(data)}(data)
    end
end
@inline BlockArray{S}(xs::Vararg{Union{Real, TensorArray}}) where {S <: Tuple} = BlockArray{S}(xs)

@generated function BlockVector(xs::Tuple{Vararg{Union{Real, TensorArray}}})
    exps = [:(Basis(xs[$i])) for i in 1:length(xs.parameters)]
    return quote
        @_inline_meta
        BlockVector{tuple($(exps...))}(xs)
    end
end
@inline BlockVector(xs::Vararg{Union{Real, TensorArray}}) = BlockVector(xs)

@pure function Base.size(x::Type{<: BlockArray{S}}) where {S}
    return tuple(map(length, S.parameters)...)
end
@inline Base.size(x::BlockArray) = size(typeof(x))
@inline Base.length(x::Type{<: BlockArray}) = prod(size(x))
@inline Base.length(x::BlockArray) = length(typeof(x))
@inline Base.LinearIndices(x::BlockArray) = LinearIndices(size(x))
@inline Base.CartesianIndices(x::BlockArray) = CartesianIndices(size(x))

@propagate_inbounds @inline function Base.getindex(x::BlockArray, ::Val{i}) where {i}
    return x.data[i]
end
@generated function Base.getindex(x::BlockArray, ::Val{i}, ::Val{j}) where {i, j}
    return quote
        @_propagate_inbounds_meta
        @_inline_meta
        x[Val($(LinearIndices(size(x))[i,j]))]
    end
end

@generated function Base.setindex!(x::BlockArray{S, T, Ts}, v::Union{Real, TensorArray}, ::Val{I}) where {S, T, Ts, I}
    1 ≤ I ≤ length(x) || return :(throw(BoundsError(x.data, I)))
    isbitstype(T) || return :(error("setindex!() with non-isbits eltype is not supported by TensorArrays"))
    ts = Ts.parameters
    basis = Basis(ts[I])
    if promote_basis(basis, Basis(v)) != basis
        return :(throw(ArgumentError("inconsistent type of argument $v")))
    end
    offset = mapreduce(i -> ncomponents(Basis(ts[i])), +, 1:I-1; init = 0)
    ex = Expr(:block, map(1:ncomponents(basis)) do i
                  v_i = isa(basis, Basis{SpaceSet()}) ? :v : getindex_expr(:v, v, tupleindexing(basis)[i])
                  return :(unsafe_store!(Base.unsafe_convert(Ptr{$T},
                                                             pointer_from_objref(x)),
                                         convert($T, $v_i),
                                         $(offset + i)))
              end...)
    return quote
        @_inline_meta
        @inbounds $ex
        return v
    end
end
@generated function Base.setindex!(x::BlockArray, v, ::Val{i}, ::Val{j}) where {i, j}
    return quote
        @_propagate_inbounds_meta
        @_inline_meta
        x[Val($(LinearIndices(size(x))[i,j]))] = v
    end
end

@generated function tomandel(x::BlockMatrix{Si, Sj, T}) where {Si, Sj, T}
    m = sum(ncomponents.(Si))
    n = sum(ncomponents.(Sj))
    range_i = BlockRange(Si)
    range_j = BlockRange(Sj)
    ex = Expr(:block)
    for k in 1:length(x)
        x_k = Symbol(:x, k)
        push!(ex.args, :($x_k = tomandel_tuple(x[$(Val(k))])))
        I, J = Tuple(CartesianIndices(size(x))[k])
        count = 1
        for j in range_j[J], i in range_i[I]
            push!(ex.args, :(M[$i, $j] = $x_k[$count]))
            count += 1
        end
    end
    return quote
        M = Matrix{T}(undef, $m, $n)
        @inbounds $ex
        return M
    end
end

@generated function frommandel(TT::Type{<: BlockMatrix{Si, Sj}}, x::AbstractMatrix) where {Si, Sj}
    m = sum(ncomponents.(Si))
    n = sum(ncomponents.(Sj))
    range_i = BlockRange(Si)
    range_j = BlockRange(Sj)
    ex = Expr(:block)
    exps = []
    BT = BlockMatrix{Si, Sj}
    for k in 1:length(BT)
        x_k = Symbol(:x, k)
        I, J = Tuple(CartesianIndices(size(BT))[k])
        args = [:(x[$i,$j]) for j in range_j[J] for i in range_i[I]]
        push!(ex.args, :($x_k = frommandel_tuple($(tensortype(Si[I] ⊗ Sj[J])), tuple($(args...)))))
        push!(exps, x_k)
    end
    return quote
        @inbounds $ex
        return TT(tuple($(exps...)))
    end
end

@inline function Base.inv(x::BlockMatrix{Si, Sj}) where {Si, Sj}
    frommandel(BlockMatrix{Si, Sj}, inv(tomandel(x)))
end

@generated function LinearAlgebra.norm(x::BlockArray{S}) where S
    exps = map(1:length(x)) do i
        sub = CartesianIndices(size(x))[i]
        basis = mapreduce(j -> S.parameters[j][sub[j]], ⊗, 1:length(sub); init = e₀)
        return basis == e₀ ?
               :(x[$(Val(i))]^2) :
               :(contract(x[$(Val(i))], x[$(Val(i))], $(Val(ndims(basis)))))
    end
    return quote
        @_inline_meta
        @inbounds return sqrt($(Expr(:call, :+, exps...)))
    end
end

@generated function Base.isapprox(x::BlockArray, y::BlockArray; kwargs...)
    size(x) != size(y) && return :(throw(DimensionMismatch("dimensions must match")))
    exps = [:(isapprox(x[$(Val(i))], y[$(Val(i))]; kwargs...)) for i in 1:length(x)]
    return quote
        @_inline_meta
        @inbounds return $(Expr(:call, :*, exps...))
    end
end

# tomandel_tuple, frommandel_tuple

@generated function tomandel_tuple(x::TensorArray{S, B, T}) where {S, B, T}
    basis = Basis(x)
    inds = tupleindexing(basis)
    dups = tupleindexdups(basis)
    ex = Expr(:block, [:($(Symbol(:v_, i)) = $T(sqrt($i))) for i in unique(dups) if i != 1]...)
    exps = map(1:ncomponents(basis)) do i
        val = getindex_expr(:x, x, inds[i])
        dup = dups[i]
        return dup == 1 ? val : :($val * $(Symbol(:v_, dup)))
    end
    return quote
        @_inline_meta
        $ex
        @inbounds return tuple($(exps...))
    end
end
@inline tomandel_tuple(x::Real) = (x,)

@generated function frommandel_tuple(::Type{TA}, x::NTuple{L, Real}) where {TA <: TensorArray, L}
    if ncomponents(TA) != L
        throw(ArgumentError("number of components must match"))
    end
    basis = Basis(TA)
    inds = tupleindexing(basis)
    dups = tupleindexdups(basis)
    T = promote_tuple_type(x)
    ex = Expr(:block, [:($(Symbol(:v_, i)) = $T(sqrt($i))) for i in unique(dups) if i != 1]...)
    exps = map(1:L) do i
        val = :(x[$i])
        dup = dups[i]
        return dup == 1 ? val : :($val / $(Symbol(:v_, dup)))
    end
    return quote
        @_inline_meta
        $ex
        @inbounds return TA(tuple($(exps...)))
    end
end
@inline frommandel_tuple(::Type{tensortype(Basis())}, x::Tuple{Real}) = @inboundsret x[1]

# BlockRange

struct BlockRange{N}
    ranges::NTuple{N, UnitRange{Int}}
end

@pure function BlockRange(spaces::NTuple{N, Basis}) where N
    ranges = ntuple(Val(N)) do I
        strt = 1
        for i in 1:I-1
            strt += ncomponents(spaces[i])
        end
        strt:(strt + ncomponents(spaces[I]) - 1)
    end
    BlockRange{N}(ranges)
end

@pure function Base.getindex(x::BlockRange{N}, i::Int) where N
    @assert 1 ≤ i ≤ N
    @inbounds return x.ranges[i]
end
