@inline function Base.map(f, x1::TensorArray, xs::TensorArray...)
    _map(f, mapreduce(Basis, promote_basis, xs; init = Basis(x1)), x1, xs...)
end

@generated function _map(f, ::Basis{S}, xs::Vararg{TensorArray, N}) where {S, N}
    basis = Basis(S)
    TensorType = tensortype(basis)
    exps = map(tupleindexing(basis)) do i
        inds = [getindex_expr(:(xs[$j]), xs[j], i) for j in 1:N]
        return :(f($(inds...)))
    end
    return quote
        @_inline_meta
        @inbounds return $TensorType(tuple($(exps...)))
    end
end
