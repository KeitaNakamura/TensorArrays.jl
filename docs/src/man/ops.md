```@meta
DocTestSetup = quote
    using Random; Random.seed!(1234)
    using TensorArrays
end
```

# Tensor operators

```@index
Pages = ["ops.md"]
Order = [:function]
```

```@autodocs
Modules = [TensorArrays]
Pages   = ["ops.jl"]
```
