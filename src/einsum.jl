using MacroTools

macro einsum(expr) # TODO: better error messages
    @assert @capture(expr, (ij__,) -> rhs_) || # :(() -> rhs) become `ij == []`
            @capture(expr, (ij__) -> rhs_)  || # for the case :(i -> rhs)
            @capture(expr, rhs_) # `ij == nothing`
    rhs = unblock(rhs)
    eachinds = Dict{Symbol, Expr}()
    allinds = Set{Symbol}()
    dummyinds = Set{Symbol}()
    assertions = Dict{Symbol, Expr}()
    signs = Expr(:tuple)
    rhs = MacroTools.postwalk(rhs) do ex
        if @capture(ex, name_Symbol_ref[inds__])
            for (i, index) in enumerate(inds)
                sz = :(size($name, $i))
                if isa(index, Symbol)
                    if index in allinds
                        push!(dummyinds, index)
                        push!(assertions[index].args, :(==), sz)
                    else
                        push!(allinds, index)
                        assertions[index] = Expr(:comparison, sz)
                    end
                    eachinds[index] = :($index = 1:$sz)
                else # index should be Int?
                    assertions[Symbol(:_, name, :_, i)] = :(0 < $index ≤ $sz)
                end
            end
            index = :($tensorindexing($name)[$(inds...)])
            push!(signs.args, :(sign($index)))
            return :(Tuple($name)[$(Expr(:$, :(abs($index))))])
        end
        return ex
    end
    freeinds = setdiff(allinds, dummyinds)

    code = Expr(:tuple, :(prod($signs)), Meta.quot(rhs))
    if !isempty(dummyinds)
        code = Expr(:typed_comprehension, Tuple{Int, Expr},
                    Expr(:generator, code, [eachinds[index] for index in sort(collect(dummyinds))]...))
    else
        code = :([$code])
    end
    code = :($reducer($code))
    if !isempty(freeinds)
        @assert freeinds == Set{Symbol}(ij)
        code = Expr(:typed_comprehension, :Expr,
                    Expr(:generator, code, [eachinds[index] for index in ij]...))
    else
        @assert ij == [] || ij == nothing
    end
    return Expr(:block, [:(@assert $ex) for ex in values(assertions) if length(ex.args) > 2]..., code) |> esc
end

function reducer(exps::Array{Tuple{Int, Expr}})
    reduce((x, y) -> :($x + $y), remove_duplicates(exps))
end

function remove_duplicates(exps::Array{Tuple{Int, Expr}})
    ret = Expr[]
    ndups = Int[]
    for (sgn, ex) in exps
        sgn == 0 && continue
        i = something(findfirst(isequal(ex), ret), 0)
        if i == 0
            push!(ret, ex)
            push!(ndups, sgn)
        else
            ndups[i] += sgn
        end
    end
    for i in 1:length(ret)
        if ndups[i] != 1
            ret[i] = :($(ndups[i]) * $(ret[i]))
        end
    end
    nzinds = findall(x -> x != 0, ndups)
    return isempty(nzinds) ? Expr[:(zero(ElType))] : ret[nzinds] # TODO: better way to handle zero value
end
