using ForwardDiff: Dual, value, partials

@generated function dualize(x::T, ::Val{N} = Val(1), ::Val{st} = Val(1)) where {T <: Real, N, st}
    ex = Expr(:block)
    N != 1 && push!(ex.args, :(z = zero(T)))
    exps = [i == st ? :(one(T)) : :z for i in 1:N]
    return quote
        @_inline_meta
        $ex
        Dual(x, tuple($(exps...)))
    end
end
@generated function dualize(x::TensorArray{S, B, T}, ::Val{N} = Val(ncomponents(Basis(x))), ::Val{st} = Val(1)) where {S, B, T, N, st}
    basis = Basis(x)
    dups = tupleindexdups(basis)
    ex = Expr(:block, [:($(Symbol(:v_, i)) = v_1 / $i) for i in unique(dups) if i != 1]...)
    n = ncomponents(basis)
    exps = map(1:n) do i
        partials = [j == st+i-1 ? Symbol(:v_, dups[i]) : :z for j in 1:N]
        :(Dual(Tuple(x)[$i], tuple($(partials...))))
    end
    return quote
        @_inline_meta
        z = zero(T)
        v_1 = one(T)
        $ex
        @inbounds return TensorArray{S, B}(tuple($(exps...)))
    end
end
@generated function dualize(x::BlockVector{S}) where S
    exps = []
    st = 1
    N = sum(ncomponents.(S))
    for i in 1:length(x)
        push!(exps, :($dualize(x[$(Val(i))], $(Val(N)), $(Val(st)))))
        st += ncomponents(S[i])
    end
    return quote
        @_inline_meta
        @inbounds return BlockVector{S}(tuple($(exps...)))
    end
end

const RealOrTensor{T <: Real} = Union{T, TensorArray{S, B, T} where {S, B}}

@inline extract_value(v::RealOrTensor, ::Union{RealOrTensor, BlockVector}) = v
@inline extract_value(v::Dual, ::Union{RealOrTensor, BlockVector}) = value(v)
@generated function extract_value(v::TensorArray{S, B, <: Dual}, ::RealOrTensor) where {S, B}
    exps = [:(value(Tuple(v)[$i])) for i in 1:ncomponents(v)]
    return quote
        @_inline_meta
        TensorType = tensortype(v)
        @inbounds return TensorType(tuple($(exps...)))
    end
end
@generated function extract_value(v::BlockVector{S, <: Dual}, x::BlockVector{S}) where {S}
    exps = [:(extract_value(v[$(Val(i))], x[$(Val(i))])) for i in 1:length(v)]
    return quote
        @_inline_meta
        @inbounds return BlockVector{S}(tuple($(exps...)))
    end
end

# Real case
@inline extract_gradient(v::RealOrTensor, ::Real, ::Val = Val(0), ::Val = Val(0)) = zero(v)
@inline function extract_gradient(v::RealOrTensor, x::TensorArray, ::Val = Val(0), ::Val = Val(0))
    TensorType = tensortype(Basis(v) ⊗ Basis(x))
    zero(TensorType{eltype(v)})
end

# Dual case
@inline function extract_gradient(v::Dual, ::Real, ::Val{st} = Val(1)) where {st}
    @inbounds return partials(v, st)
end
@generated function extract_gradient(v::Dual, x::TensorArray, ::Val{st} = Val(1)) where {st}
    TensorType = tensortype(x)
    exps = [:(partials(v, $i)) for i in st:st+ncomponents(x)-1]
    return quote
        @_inline_meta
        @inbounds return $TensorType(tuple($(exps...)))
    end
end
@generated function extract_gradient(v::TensorArray{S, B, <: Dual}, x::TensorArray, ::Val{st} = Val(1)) where {S, B, st}
    TensorType = tensortype(Basis(v) ⊗ Basis(x))
    exps = [:(partials(Tuple(v)[$i], $j)) for i in 1:ncomponents(v), j in st:st+ncomponents(x)-1]
    return quote
        @_inline_meta
        @inbounds return $TensorType(tuple($(exps...)))
    end
end
@generated function extract_gradient(v::TensorArray{S, B, <: Dual}, ::Real, ::Val{st} = Val(1)) where {S, B, st}
    TensorType = tensortype(v)
    exps = [:(partials(Tuple(v)[$i], st)) for i in 1:ncomponents(v)]
    return quote
        @_inline_meta
        @inbounds return $TensorType(tuple($(exps...)))
    end
end

@generated function extract_gradient(v::RealOrTensor, x::BlockVector{S}) where S
    exps = []
    st = 1
    for i in 1:length(x)
        push!(exps, :(extract_gradient(v, x[$(Val(i))], $(Val(st)))))
        st += ncomponents(S[i])
    end
    spaces = map(x -> Basis(v) ⊗ x, S)
    return quote
        @_inline_meta
        @inbounds return BlockVector{$spaces}(tuple($(exps...)))
    end
end
@generated function extract_gradient(v::BlockVector{S, <: Real}, x::BlockVector{S}) where S
    exps = []
    N = length(x)
    st = 1
    for j in 1:N
        for i in 1:N
            push!(exps, :(extract_gradient(v[$(Val(i))], x[$(Val(j))], $(Val(st)))))
        end
        st += ncomponents(S[j])
    end
    return quote
        @_inline_meta
        @inbounds return BlockMatrix{S, S}(tuple($(exps...)))
    end
end

function LinearAlgebra.gradient(f::F, x::Union{Real, TensorArray, BlockVector}) where F
    dx = dualize(x)
    v = f(dx)
    extract_gradient(v, x)
end
const ∇ = gradient

function LinearAlgebra.gradient(f::F, x::Union{Real, TensorArray, BlockVector}, ::Symbol) where F
    dx = dualize(x)
    v = f(dx)
    extract_gradient(v, x), extract_value(v, x)
end

function hessian(f::F, x::Union{Real, TensorArray, BlockVector}) where F
    dx² = dualize(dualize(x))
    v = f(dx²)
    extract_gradient(v, x)
end
const ∇² = hessian

function hessian(f::F, x::Union{Real, TensorArray, BlockVector}, ::Symbol) where F
    dx² = dualize(dualize(x))
    v = f(dx²)
    v2 = extract_value(v2, x)
    extract_gradient(v, x), extract_gradient(v2, x), extract_value(v2, x)
end
