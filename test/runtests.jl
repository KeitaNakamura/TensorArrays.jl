using LinearAlgebra, Statistics
using TensorArrays
using StaticArrays
using Combinatorics
using Test
using Random

using TensorArrays: Space, SymmetricSpace, AntisymmetricSpace, SpaceSet, promote_space, droplast, dropfirst
using TensorArrays: tensorindexing, tupleindexing, tuplesubindexing, tensortype,
                    contract, promote_basis, levicivita,
                    tomandel_tuple, frommandel_tuple,
                    TensorArrayNoEltype, promote_tensor

macro eachtensor(TensorTypes, Ns, dims, Ts)
    @assert Meta.isexpr(TensorTypes, :tuple)
    @assert Meta.isexpr(Ns, :tuple)
    @assert Meta.isexpr(dims, :tuple)
    @assert Meta.isexpr(Ts, :tuple)
    exps = []
    for TensorType in TensorTypes.args, N in Ns.args, dim in dims.args, T in Ts.args
        @assert isa(TensorType, Symbol)
        @assert isa(N, Int)
        @assert isa(dim, Int)
        @assert isa(T, Symbol)
        if TensorType == :SymmetricTensor
            rem(N, 2) == 0 && 2 ≤ N || continue
        elseif TensorType == :FullySymmetricTensor
            2 ≤ N || continue
        elseif TensorType == :AntisymmetricTensor
            rem(N, 2) == 0 && 2 ≤ N && 2 ≤ dim || continue
        elseif TensorType == :FullyAntisymmetricTensor
            2 ≤ N ≤ dim || continue
        end
        push!(exps, :(($TensorType, $N, $dim, $T)))
    end
    return esc(:(tuple($(exps...))))
end
macro eachtensor(Ns, dims, Ts)
    esc(:(@eachtensor((Tensor, SymmetricTensor, AntisymmetricTensor, FullySymmetricTensor, FullyAntisymmetricTensor),
                      $Ns, $dims, $Ts)))
end

@generated function contract_array(x::TensorArray{S_x, <: Tuple, <: Real, N_x},
                                   y::TensorArray{S_y, <: Tuple, <: Real, N_y}, ::Val{n}) where {S_x, S_y, N_x, N_y, n}
    dims_x = size(x)
    dims_y = size(y)
    I = tuple([dims_y[i] for i in 1:n]...)
    promote_shape(tuple([dims_x[i] for i in N_x-n+1:N_x]...), I)
    L = tuple([dims_x[i] for i in 1:N_x-n]...)
    R = tuple([dims_y[i] for i in 1+n:N_y]...)
    inner = prod(I)
    return quote
        @inbounds z = SArray{Tuple{$(prod(L)), $inner}}(tuple($([:(x[$i]) for i in 1:length(x)]...))) *
                      SArray{Tuple{$inner, $(prod(R))}}(tuple($([:(y[$i]) for i in 1:length(y)]...)))
        return reshape(z, $(Size(L..., R...)))
    end
end
@inline function contract_array(x::TensorArray{S, <: Tuple, <: Real, N},
                                y::TensorArray{S, <: Tuple, <: Real, N}, ::Val{N}) where {S, N}
    dot(Array(x), Array(y))
end

include("space.jl")
include("tensors.jl")
include("voigt.jl")
include("ops.jl")
include("ad.jl")
include("block.jl")

# Build the docs
include("../docs/make.jl")
