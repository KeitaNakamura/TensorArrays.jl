@testset "Constructors" begin

# scalar-like
@test TensorArray{Tuple{}, Tuple{}, Int, 0, 1}((1.0,)).data === (1,)

@testset "$(nameof(SpaceType))" for SpaceType in (Space{2}, SymmetricSpace{2,2}, AntisymmetricSpace{2,2})
    L = ncomponents(Basis(Tuple{2,2}, Tuple{SpaceType}))
    L_bad = L+1
    L² = L^2
    @testset "with tuple" begin
        data = ntuple(identity, L)
        data_bad = ntuple(identity, L_bad)
        @test TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2, L}(data).data === Float64.(data)
        @test TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2}(data).data === Float64.(data)
        @test TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64}(data).data === Float64.(data)
        @test TensorArray{Tuple{2,2}, Tuple{SpaceType}}(data).data === data
        @test TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2, L}(data).data === data
        @test TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2}(data).data === data
        @test TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}}(data).data === data

        # Bad input
        @test_throws Exception TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int, 2, L}(data_bad)
        @test_throws Exception TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int, 2}(data_bad)
        @test_throws Exception TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int}(data_bad)
        @test_throws Exception TensorArray{Tuple{2,2}, Tuple{SpaceType}}(data_bad)
        @test_throws Exception TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2, L}(data_bad)
        @test_throws Exception TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2}(data_bad)
        @test_throws Exception TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}}(data_bad)

        # Bad parameters
        @test_throws ArgumentError promote_tensor(TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int, 2, L_bad})
        @test_throws ArgumentError promote_tensor(TensorArray{Tuple{2,2,2,2}, Tuple{SpaceType, SpaceType}, Int, 2, L²})
        @test_throws ArgumentError promote_tensor(TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2, L_bad})
        @test_throws ArgumentError promote_tensor(TensorArrayNoEltype{Tuple{2,2,2,2}, Tuple{SpaceType, SpaceType}, 2, L²})
    end
    @testset "with function" begin
        f = (ij...) -> 1
        g = i -> 1
        data = ntuple(i -> 1, L)
        @test (@inferred TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2, L}(f)).data === Float64.(data)
        @test (@inferred TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2}(f)).data === Float64.(data)
        @test (@inferred TensorArray{Tuple{2,2}, Tuple{SpaceType}}(f)).data === data
        @test (@inferred TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2, L}(f)).data === data
        @test (@inferred TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2}(f)).data === data
        @test (@inferred TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}}(f)).data === data

        # Bad input
        @test_throws Exception TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2, L}(g)
        @test_throws Exception TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2}(g)
        @test_throws Exception TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64}(g)
        @test_throws Exception TensorArray{Tuple{2,2}, Tuple{SpaceType}}(g)
    end
    @testset "with array" begin
        A = reshape(1:4, (2,2))
        res = if SpaceType <: Space; A
        elseif SpaceType   <: SymmetricSpace; (A + A')/2
        elseif SpaceType   <: AntisymmetricSpace; (A - A')/2
        else error() end
        @test (@inferred TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2, L}(A))::TensorArray{} == res
        @test (@inferred TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2}(A)) == res
        @test (@inferred TensorArray{Tuple{2,2}, Tuple{SpaceType}}(A)) == res
        @test (@inferred TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2, L}(A)) == res
        @test (@inferred TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2}(A)) == res
        @test (@inferred TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}}(A)) == res

        # Bad input
        @test_throws Exception TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2, L}(zeros(1))
        @test_throws Exception TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2}(zeros(1))
        @test_throws Exception TensorArray{Tuple{2,2}, Tuple{SpaceType}}(zeros(1))
        @test_throws Exception TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2, L}(zeros(1))
        @test_throws Exception TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2}(zeros(1))
        @test_throws Exception TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}}(zeros(1))
    end
    @testset "rand" begin
        (@inferred rand(TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int, 2, L}))::TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int, 2, L}
        (@inferred rand(TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int, 2}))::TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int, 2, L}
        (@inferred rand(TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int}))::TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int, 2, L}
        (@inferred rand(TensorArray{Tuple{2,2}, Tuple{SpaceType}}))::TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2, L}
        (@inferred rand(TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2, L}))::TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2, L}
        (@inferred rand(TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2}))::TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2, L}
        (@inferred rand(TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}}))::TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2, L}
    end
    @testset "$op" for (op, el) in ((ones, 1), (zero, 0))
        data = ntuple(i -> el, L)
        @test (@inferred op(TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int, 2, L})).data === data
        @test (@inferred op(TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int, 2})).data === data
        @test (@inferred op(TensorArray{Tuple{2,2}, Tuple{SpaceType}, Int})).data === data
        @test (@inferred op(TensorArray{Tuple{2,2}, Tuple{SpaceType}})).data === Float64.(data)
        @test (@inferred op(TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2, L})).data === Float64.(data)
        @test (@inferred op(TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2})).data === Float64.(data)
        @test (@inferred op(TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}})).data === Float64.(data)
    end
    @testset "fill" begin
        f = i -> 1
        data = ntuple(f, L)
        @test (@inferred fill(1, TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2, L})).data === Float64.(data)
        @test (@inferred fill(1, TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2})).data === Float64.(data)
        @test (@inferred fill(1, TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2, L})).data === data
        @test (@inferred fill(1, TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2})).data === data
        @test (@inferred fill(1, TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}})).data === data
        @test (@inferred fill(f, TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2, L})).data === Float64.(data)
        @test (@inferred fill(f, TensorArray{Tuple{2,2}, Tuple{SpaceType}, Float64, 2})).data === Float64.(data)
        @test (@inferred fill(f, TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2, L})).data === data
        @test (@inferred fill(f, TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}, 2})).data === data
        @test (@inferred fill(f, TensorArrayNoEltype{Tuple{2,2}, Tuple{SpaceType}})).data === data
    end
end
@testset "Tensor aliases" begin
    for T in (Float32, Float64, Int)
        @testset "$TensorType" for TensorType in (Vec{3}, Tensor{2,3}, SymmetricTensor{2,3}, AntisymmetricTensor{2,3}, STensor{Tuple{2,3}}, FullySymmetricTensor{3,3}, FullyAntisymmetricTensor{3,3})
        L = ncomponents(TensorType)
        data = ntuple(i -> i, L)
        @test ((@inferred TensorType(data))::TensorType).data === data
        @test ((@inferred TensorType{T}(data))::TensorType{T}).data === T.(data)
    end
    end
end
@testset "symmetric/antisymmetric type exceptions" begin
    # symmetric
    @test_throws ArgumentError promote_tensor(TensorArray{Tuple{2,2}, Tuple{SymmetricSpace{2}}, Int, 2, 3})
    @test_throws ArgumentError promote_tensor(TensorArray{Tuple{2,2}, Tuple{SymmetricSpace{1,2}}, Int, 2, 3})
    @test_throws ArgumentError promote_tensor(TensorArray{Tuple{2,2}, Tuple{SymmetricSpace{2,1}}, Int, 2, 3})
    @test_throws ArgumentError promote_tensor(TensorArray{Tuple{2,1}, Tuple{SymmetricSpace{2,2}}, Int, 2, 3})
    @test_throws ArgumentError promote_tensor(TensorArray{Tuple{2,2,2,2}, Tuple{SymmetricSpace{2,2}, SymmetricSpace{2,2}}, Int, 4, 9})
    # antisymmetric
    @test_throws ArgumentError promote_tensor(TensorArray{Tuple{2,2}, Tuple{AntisymmetricSpace{2}}, Int, 2, 1})
    @test_throws ArgumentError promote_tensor(TensorArray{Tuple{2,2}, Tuple{AntisymmetricSpace{1,2}}, Int, 2, 1})
    @test_throws ArgumentError promote_tensor(TensorArray{Tuple{2,2}, Tuple{AntisymmetricSpace{2,1}}, Int, 2, 1})
    @test_throws ArgumentError promote_tensor(TensorArray{Tuple{2,1}, Tuple{AntisymmetricSpace{2,2}}, Int, 2, 1})
    @test_throws ArgumentError promote_tensor(TensorArray{Tuple{1,1}, Tuple{AntisymmetricSpace{2,2}}, Int, 2})
    @test_throws ArgumentError promote_tensor(TensorArray{Tuple{2,2,2,2}, Tuple{AntisymmetricSpace{2,2}, AntisymmetricSpace{2,2}}, Int, 4, 1})
end
@testset "one" begin
    for T in (Float32, Float64), dim in (2, 3)
        (@inferred one(Tensor{2, dim}))::Tensor{2, dim, Float64}
        (@inferred one(Tensor{4, dim}))::Tensor{4, dim, Float64}
        (@inferred one(SymmetricTensor{2, dim}))::SymmetricTensor{2, dim, Float64}
        (@inferred one(SymmetricTensor{4, dim}))::SymmetricTensor{4, dim, Float64}
        (@inferred one(AntisymmetricTensor{4, dim}))::AntisymmetricTensor{4, dim, Float64}
        I      = (@inferred one(Tensor{2, dim, T}))::Tensor{2, dim, T}
        II     = (@inferred one(Tensor{4, dim, T}))::Tensor{4, dim, T}
        I_sym  = (@inferred one(SymmetricTensor{2, dim, T}))::SymmetricTensor{2, dim, T}
        II_sym = (@inferred one(SymmetricTensor{4, dim, T}))::SymmetricTensor{4, dim, T}
        II_ant = (@inferred one(AntisymmetricTensor{4, dim, T}))::AntisymmetricTensor{4, dim, T}
        @test I      == (@inferred one(I))::Tensor{2, dim, T}
        @test II     == (@inferred one(II))::Tensor{4, dim, T}
        @test I_sym  == (@inferred one(I_sym))::SymmetricTensor{2, dim, T}
        @test II_sym == (@inferred one(II_sym))::SymmetricTensor{4, dim, T}
        @test II_ant == (@inferred one(II_ant))::AntisymmetricTensor{4, dim, T}
        x = rand(Tensor{2, dim})
        @test II_sym ⊡ x ≈ (x + x') / 2
        @test II_ant ⊡ x ≈ (x - x') / 2
        for i in 1:dim, j in 1:dim
            if i == j
                @test I[i,j] == I_sym[i,j] == T(1)
            else
                @test I[i,j] == I_sym[i,j] == T(0)
            end
            for k in 1:dim, l in 1:dim
                if i == k && j == l
                    @test II[i,j,k,l] == T(1)
                    if i == l && j == k
                        @test II_sym[i,j,k,l] == T(1)
                        @test II_ant[i,j,k,l] == T(0)
                    else
                        @test II_sym[i,j,k,l] == T(1) / 2
                        @test II_ant[i,j,k,l] == T(1) / 2
                    end
                else
                    @test II[i,j,k,l] == T(0)
                    if i == l && j == k
                        @test II_sym[i,j,k,l] ==  T(1) / 2
                        @test II_ant[i,j,k,l] == -T(1) / 2
                    else
                        @test II_sym[i,j,k,l] == T(0)
                        @test II_ant[i,j,k,l] == T(0)
                    end
                end
            end
        end
    end
    for T in (Float32, Float64, Int)
        (@inferred one(LeviCivita))::LeviCivita{Float64}
        ϵ = (@inferred one(LeviCivita{T}))::LeviCivita{T}
        @test ϵ ≈ (@inferred one(ϵ))::LeviCivita{T}
        for l in 1:27
            i, j, k = Tuple(CartesianIndices((3,3,3))[l])
            if (i,j,k) == (1,2,3) || (i,j,k) == (2,3,1) || (i,j,k) == (3,1,2)
                @test ϵ[i,j,k] == T(1)
            elseif (i,j,k) == (2,1,3) || (i,j,k) == (3,2,1) || (i,j,k) == (1,3,2)
                @test ϵ[i,j,k] == -T(1)
            else
                @test ϵ[i,j,k] == T(0)
            end
        end
    end
end

end
