@testset "Spaces/SpaceSet" begin
    @testset "Space constructors" begin
        @testset "Space" begin
            @test Space(1,2).dims === (1,2)
        end
        @testset "$SpaceType" for SpaceType in (SymmetricSpace, AntisymmetricSpace)
            @test SpaceType(2,2).dims === (2,2)
            @test SpaceType{2}(2,2,3,3).dims === (2,2,3,3)

            # Bad input
            @test_throws Exception SpaceType(2)
            @test_throws Exception SpaceType(2,3)
            @test_throws Exception SpaceType{2}(2,2,2,3)

            # Bad parameters
            @test_throws Exception SpaceType{1}(2)
            @test_throws Exception SpaceType{1}(2,2)
            @test_throws Exception SpaceType{3}(2,2,2,2)
            if SpaceType <: AntisymmetricSpace
                @test_throws Exception SpaceType(2,2,2)
            end
        end
    end
    @testset "SpaceSet" begin
        # constructors
        @test (@inferred SpaceSet(Space(3), Space(3), Space(3))).spaces === (Space(3,3,3),)
        for SpaceType in (SymmetricSpace, AntisymmetricSpace)
            @test (@inferred SpaceSet(SpaceType(2,2), SpaceType(3,3), SpaceType(2,2))).spaces === (SpaceType{2}(2,2,3,3,2,2),)
            @test (@inferred SpaceSet(SpaceType(3,3,3), SpaceType(2,2))).spaces === (SpaceType(3,3,3), SpaceType(2,2))
        end
        @test (@inferred SpaceSet(Space(3), SymmetricSpace(2,2))).spaces === (Space(3), SymmetricSpace(2,2))
        @test (@inferred SpaceSet(Space(3), AntisymmetricSpace(2,2))).spaces === (Space(3), AntisymmetricSpace(2,2))
        @test (@inferred SpaceSet(SymmetricSpace(2,2), AntisymmetricSpace(2,2))).spaces === (SymmetricSpace(2,2), AntisymmetricSpace(2,2))

        # split
        @test (@inferred split(SpaceSet(Space(3,3,3)))) === (Space(3), Space(3), Space(3))
        @test (@inferred split(SpaceSet(Space(3,3,3), Space(3,3)))) === (Space(3), Space(3), Space(3), Space(3), Space(3))
        for SpaceType in (SymmetricSpace, AntisymmetricSpace)
            @test (@inferred split(SpaceSet(SpaceType{2}(2,2,3,3,2,2)))) === (SpaceType(2,2), SpaceType(3,3), SpaceType(2,2))
        end

        # promote_space
        @test (@inferred promote_space(SpaceSet(Space(3,3)), SpaceSet(Space(3,3)))) === SpaceSet(Space(3,3))
        for SpaceType in (SymmetricSpace, AntisymmetricSpace)
            @test (@inferred promote_space(SpaceSet(Space(3,3)), SpaceSet(SpaceType(3,3)))) === SpaceSet(Space(3,3))
            @test (@inferred promote_space(SpaceSet(Space(3),SpaceType(3,3)), SpaceSet(SpaceType(3,3,3)))) === SpaceSet(Space(3),SpaceType(3,3))
            @test_throws Exception promote_space(SpaceSet(Space(3,2)), SpaceSet(SpaceType(3,3)))
        end

        # symmetric/antisymmetric
        @test (@inferred symmetric(SpaceSet(Space(3,3)))) === SpaceSet(SymmetricSpace(3,3))
        @test (@inferred antisymmetric(SpaceSet(Space(3,3)))) === SpaceSet(AntisymmetricSpace(3,3))
        for SpaceType in (SymmetricSpace, AntisymmetricSpace)
            @test_throws Exception symmetric(SpaceSet(SpaceType(3,3)))
            @test_throws Exception antisymmetric(SpaceSet(SpaceType(3,3)))
        end

        # ncomponents/size/ndims
        @test (@inferred ncomponents(SpaceSet(Space(1,2,3)))) === 6
        @test (@inferred size(SpaceSet(Space(1,2,3)))) === (1,2,3)
        @test (@inferred ndims(SpaceSet(Space(1,2,3)))) === 3
        @test (@inferred ncomponents(SpaceSet())) === 1 # scalar-like
        @test (@inferred size(SpaceSet())) === ()       # scalar-like
        @test (@inferred ndims(SpaceSet())) === 0       # scalar-like
        for SpaceType in (SymmetricSpace, AntisymmetricSpace)
            @test (@inferred size(SpaceSet(Space(2,3), SpaceType(3,3)))) === (2,3,3,3)
            @test (@inferred ndims(SpaceSet(Space(2,3), SpaceType(3,3)))) === 4
        end

        # dropfirst/droplast
        @test (@inferred droplast(SpaceSet(Space(1,2,3)))) === SpaceSet(Space(1,2))
        @test (@inferred dropfirst(SpaceSet(Space(1,2,3)))) === SpaceSet(Space(2,3))
        for SpaceType in (SymmetricSpace, AntisymmetricSpace)
            @test (@inferred droplast(SpaceSet(SpaceType(3,3)))) === SpaceSet(Space(3))
            @test (@inferred droplast(SpaceSet(SpaceType(3,3,3)))) === SpaceSet(SpaceType(3,3))
            @test (@inferred droplast(SpaceSet(SpaceType{2}(2,2,3,3)))) === SpaceSet(SpaceType(2,2),Space(3))
            @test (@inferred droplast(SpaceSet(SpaceType(3,3),Space(2)))) === SpaceSet(SpaceType(3,3))
            @test (@inferred dropfirst(SpaceSet(SpaceType(3,3)))) === SpaceSet(Space(3))
            @test (@inferred dropfirst(SpaceSet(SpaceType(3,3,3)))) === SpaceSet(SpaceType(3,3))
            @test (@inferred dropfirst(SpaceSet(SpaceType{2}(2,2,3,3)))) === SpaceSet(Space(2),SpaceType(3,3))
            @test (@inferred dropfirst(SpaceSet(SpaceType(3,3),Space(2)))) === SpaceSet(Space(3,2))
        end
    end
end
