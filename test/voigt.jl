@testset "voigt" begin
    for dim in (1,2,3), T in (Float32, Float64)
        A = rand(Tensor{2, dim, T})
        AA = rand(Tensor{4, dim, T})
        A_sym = rand(SymmetricTensor{2, dim, T})
        AA_sym = rand(SymmetricTensor{4, dim, T})
        T2 = T(2)
        # tovoigt
        @test (@inferred tovoigt(AA)) * (@inferred tovoigt(A)) ≈ tovoigt(AA ⊡ A)
        @test (@inferred tovoigt(AA_sym)) * (@inferred tovoigt(A_sym, offdiagscale = T2)) ≈ tovoigt(AA_sym ⊡ A_sym)
        # fromvoigt
        @test (@inferred fromvoigt(Tensor{2, dim}, tovoigt(A))) ≈ A
        @test (@inferred fromvoigt(Tensor{4, dim}, tovoigt(AA))) ≈ AA
        @test (@inferred fromvoigt(SymmetricTensor{2, dim}, tovoigt(A_sym, offdiagscale = T2), offdiagscale = T2)) ≈ A_sym
        @test (@inferred fromvoigt(SymmetricTensor{4, dim}, tovoigt(AA_sym, offdiagscale = T2), offdiagscale = T2)) ≈ AA_sym
        # tomandel
        @test (@inferred tomandel(AA_sym)) * (@inferred tomandel(A_sym)) ≈ tomandel(AA_sym ⊡ A_sym)
        # frommandel
        @test (@inferred frommandel(SymmetricTensor{2, dim}, tomandel(A_sym))) ≈ A_sym
        @test (@inferred frommandel(SymmetricTensor{4, dim}, tomandel(AA_sym))) ≈ AA_sym
    end
end
