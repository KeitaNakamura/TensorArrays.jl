module TensorArrays

using LinearAlgebra, Statistics
# re-exports from LinearAlgebra
export ⋅, ×, dot, diagm, tr, det, norm

using Base: @_inline_meta, @_pure_meta, @pure, @_propagate_inbounds_meta, @propagate_inbounds
using StaticArrays

export TensorArray, Vec, Tensor, SymmetricTensor, AntisymmetricTensor,
       FullySymmetricTensor, FullyAntisymmetricTensor, STensor,
       LeviCivita, Scalar, BlockVector, BlockMatrix
export Basis
export contract
export symmetric, antisymmetric
export vol, dev
export gradient, hessian
export tovoigt, tomandel, fromvoigt, frommandel
export ncomponents
export ⊗, ⊡
export e₀, e₁, e₂, e₃, eᵢ

include("utils.jl")
include("spaces.jl")
include("basis.jl")
include("tensors.jl")
include("einsum.jl")
include("map.jl")
include("voigt.jl")
include("ops.jl")
include("block.jl")
include("ad.jl")

const ⊗ = otimes
const ⊡ = dcontract
const ∇ = gradient
const ∇² = hessian

const e₀ = Basis()
const e₁ = eᵢ(1)
const e₂ = eᵢ(2)
const e₃ = eᵢ(3)

end # module
